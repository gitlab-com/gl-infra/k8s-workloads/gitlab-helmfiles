---
clusterName: {{ .Values.cluster }}

externalSecrets:
  gitaly-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/gitaly/secret"
          property: token
          version: "1"
        secretKey: token

  google-oauth2-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/auth/google-oauth2"
          property: provider
          version: "1"
        secretKey: provider

  kas-secret-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/kas/api"
          property: api-secret
          version: "2"
        secretKey: api-secret

  kas-private-api-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/kas/private-api"
          property: secret
          version: "1"
        secretKey: secret

  kas-websocket-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/kas/websocket"
          property: token
          version: "1"
        secretKey: token

  postgres-credentials-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/postgres/credentials"
          property: password
          version: "1"
        secretKey: password

  rails-secrets-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          secrets.yml: |
            production:
              secret_key_base: {{ `{{ "{{" }}` }} .secret_key_base | quote }}
              otp_key_base: {{ `{{ "{{" }}` }} .otp_key_base | quote }}
              db_key_base: {{ `{{ "{{" }}` }} .db_key_base | quote }}
              openid_connect_signing_key: |
                {{ `{{ "{{-" }}` }} .openid_connect_signing_key | nindent 4 }}
              active_record_encryption_primary_key:
                - {{ `{{ "{{" }}` }} .active_record_encryption_primary_key | quote }}
              active_record_encryption_deterministic_key:
                - {{ `{{ "{{" }}` }} .active_record_encryption_deterministic_key | quote }}
              active_record_encryption_key_derivation_salt: {{ `{{ "{{" }}` }} .active_record_encryption_key_derivation_salt | quote }}
    data:
      - remoteRef:
          key: env/ops/ns/{{ .Release.Namespace }}/rails/secrets
          property: secret_key_base
          version: "2"
        secretKey: secret_key_base
      - remoteRef:
          key: env/ops/ns/{{ .Release.Namespace }}/rails/secrets
          property: otp_key_base
          version: "2"
        secretKey: otp_key_base
      - remoteRef:
          key: env/ops/ns/{{ .Release.Namespace }}/rails/secrets
          property: db_key_base
          version: "2"
        secretKey: db_key_base
      - remoteRef:
          key: env/ops/ns/{{ .Release.Namespace }}/rails/secrets
          property: encrypted_settings_key_base
          version: "2"
        secretKey: encrypted_settings_key_base
      - remoteRef:
          key: env/ops/ns/{{ .Release.Namespace }}/rails/secrets
          property: openid_connect_signing_key
          version: "2"
        secretKey: openid_connect_signing_key
      - remoteRef:
          key: env/ops/ns/{{ .Release.Namespace }}/rails/secrets
          property: active_record_encryption_primary_key
          version: "2"
        secretKey: active_record_encryption_primary_key
      - remoteRef:
          key: env/ops/ns/{{ .Release.Namespace }}/rails/secrets
          property: active_record_encryption_deterministic_key
          version: "2"
        secretKey: active_record_encryption_deterministic_key
      - remoteRef:
          key: env/ops/ns/{{ .Release.Namespace }}/rails/secrets
          property: active_record_encryption_key_derivation_salt
          version: "2"
        secretKey: active_record_encryption_key_derivation_salt

  rails-storage-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          connection: |
            provider: Google
            google_project: {{ .Values.google_project }}
            google_json_key_string: |
              {{ `{{ "{{-" }}` }} .service_account_key | nindent 2 }}
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/google"
          property: service_account_key
          version: "1"
        secretKey: service_account_key

  redis-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/redis/secret"
          property: secret
          version: "1"
        secretKey: secret

  registry-certificate-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/registry/certificate"
          property: registry-auth.crt
          version: "1"
        secretKey: registry-auth.crt
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/registry/certificate"
          property: registry-auth.key
          version: "1"
        secretKey: registry-auth.key

  registry-httpsecret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/registry/httpsecret"
          property: secret
          version: "1"
        secretKey: secret

  registry-postgres-credentials-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/registry/postgresql"
          property: password
          version: "1"
        secretKey: password

  registry-storage-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          config: |
            gcs:
              bucket: gitlab-ops-registry
              keyfile: /etc/docker/registry/storage/gcs.json
          gcs.json: |
            {{ `{{ "{{-" }}` }} .service_account_key -}}
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/google"
          property: service_account_key
          version: "1"
        secretKey: service_account_key

  saml-v5:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/auth/saml"
          property: provider
          version: "5"
        secretKey: provider

  shell-host-keys-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      {{- range $alg := tuple "dsa" "ecdsa" "ed25519" "rsa" }}
      - remoteRef:
          key: "env/ops/ns/{{ $.Release.Namespace }}/shell/host-keys"
          property: ssh_host_{{ $alg }}_key
          version: "1"
        secretKey: ssh_host_{{ $alg }}_key
      - remoteRef:
          key: "env/ops/ns/{{ $.Release.Namespace }}/shell/host-keys"
          property: ssh_host_{{ $alg }}_key.pub
          version: "1"
        secretKey: ssh_host_{{ $alg }}_key.pub
      {{- end }}

  shell-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/shell/secret"
          property: secret
          version: "1"
        secretKey: secret

  smtp-password-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/smtp"
          property: password
          version: "1"
        secretKey: password

  storage-config-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/google"
          property: service_account_key
          version: "1"
        secretKey: service_account_key

  suggested-reviewers-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/suggested-reviewers/secret"
          property: secret
          version: "1"
        secretKey: secret

  workhorse-secret-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "env/ops/ns/{{ .Release.Namespace }}/workhorse/secret"
          property: shared_secret
          version: "1"
        secretKey: shared_secret

secretStores:
  - name: gitlab-secrets
    role: gitlab

serviceAccount:
  name: gitlab-secrets
