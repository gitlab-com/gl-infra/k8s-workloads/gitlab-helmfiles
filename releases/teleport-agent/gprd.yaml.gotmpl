---

# Comma-separated list of roles to enable (any of: kube,db,app)
# Our current Teleport license does not include Kubernetes.
# See https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/24944
# Currently, we are only using Database service of Teleport agents running in our Kubernetes clusters
# Enabling Kubernetes service causes the Teleport agent readiness check fails and they run in degraded mode.
roles: db

extraVolumes:
  - name: db-certs
    secret:
      secretName: db-certs-v2

extraVolumeMounts:
  - name: db-certs
    mountPath: /etc/teleport/db-certs

resources:
  requests:
    cpu: 100m
    memory: 200Mi

databases:
  - name: db-main-replica-gprd
    description: GPRD PostgreSQL Main DB replica
    protocol: postgres
    uri: patroni-main-v16-104-db-gprd.c.gitlab-production.internal:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readonly
      db-type: main

  - name: db-main-primary-gprd
    description: GPRD PostgreSQL Main DB primary
    protocol: postgres
    uri: master.patroni.service.consul:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readwrite
      db-type: main

  - name: db-ci-replica-gprd
    description: GPRD PostgreSQL CI DB replica
    protocol: postgres
    uri: patroni-ci-v16-02-db-gprd.c.gitlab-production.internal:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readonly
      db-type: ci

  - name: db-ci-primary-gprd
    description: GPRD PostgreSQL CI DB primary
    protocol: postgres
    uri: master.patroni-ci.service.consul:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readwrite
      db-type: ci

  - name: db-registry-replica-gprd
    description: GPRD PostgreSQL Registry DB replica
    protocol: postgres
    uri: patroni-registry-v16-02-db-gprd.c.gitlab-production.internal:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readonly
      db-type: registry

  - name: db-registry-primary-gprd
    description: GPRD PostgreSQL Registry DB primary
    protocol: postgres
    uri: master.patroni-registry.service.consul:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readwrite
      db-type: registry

  - name: db-main-dr-archive-gprd
    description: GPRD PostgreSQL Main DB delayed replica archive
    protocol: postgres
    uri: postgres-dr-main-v16-archive-01-db-gprd.c.gitlab-production.internal:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readonly
      db-type: main

  - name: db-ci-dr-archive-gprd
    description: GPRD PostgreSQL CI DB delayed replica archive
    protocol: postgres
    uri: postgres-ci-dr-archive-v16-01-db-gprd.c.gitlab-production.internal:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readonly
      db-type: main

  - name: db-registry-dr-archive-gprd
    description: GPRD PostgreSQL Registry DB delayed replica archive
    protocol: postgres
    uri: postgres-registry-dr-archive-01-db-gprd.c.gitlab-production.internal:5432
    tls:
      mode: verify-ca
      ca_cert_file: /etc/teleport/db-certs/patroni.crt
    static_labels:
      environment: gprd
      writable: readonly
      db-type: registry
