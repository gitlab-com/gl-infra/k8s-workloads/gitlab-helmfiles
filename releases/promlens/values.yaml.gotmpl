---
replicaCount: 2
args:
  - --web.external-url=https://promlens.gitlab.net
  - --grafana.url=http://grafana.grafana.svc.cluster.local
  - --grafana.api-token-file=/grafana/token
extraVolumes:
  - name: grafana
    secret:
      secretName: grafana-token
extraVolumeMounts:
  - name: grafana
    mountPath: /grafana
    readOnly: true
service:
  type: ClusterIP
  annotations:
    cloud.google.com/backend-config: '{"default": "promlens-backendconfig"}'
    cloud.google.com/neg: '{"ingress": true}'
google:
  backendConfig:
    enabled: true
    name: promlens-backendconfig
    config:
      healthCheck:
        checkIntervalSec: 10
        port: 8080
        requestPath: /
        timeoutSec: 5
        type: HTTP
      iap:
        enabled: true
        oauthclientCredentials:
          secretName: promlens-iap-client-creds
      timeoutSec: 182
  managedCertificate:
    enabled: true
    name: promlens-cert
    domains:
      - promlens.gitlab.net
ingress:
  enabled: true
  hostname: promlens.gitlab.net
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "promlens-ingress-{{ .Environment.Name }}"
    networking.gke.io/managed-certificates: "promlens-cert"
  path: '/*'