{
  // function to translate the job Name to camelCase (only works for jobs with dashes)
  camelCase(str)::
    local parts = std.split(str, '-');
    local capitalize(word) =
      std.asciiUpper(std.substr(word, 0, 1)) +
      std.asciiLower(std.substr(word, 1, std.length(word) - 1));
    capitalize(parts[0]) + std.join('', [capitalize(w) for w in parts[1:]]),

}
