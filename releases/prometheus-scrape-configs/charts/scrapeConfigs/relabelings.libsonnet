{
  // Only keep targets that match the regexPattern
  filterByService(name, regexPattern):: [
    {
      action: 'keep',
      sourceLabels: ['__meta_gce_label_gitlab_com_service'],
      regex: regexPattern,
    },
  ],
  // Only keep targets that match the regexPattern
  filterByServiceAndType(name, regexPattern):: [
    {
      action: 'keep',
      sourceLabels: ['__meta_gce_label_gitlab_com_service', '__meta_gce_label_gitlab_com_type'],
      regex: regexPattern,
    },
  ],
  filterByCustomLabels(regexPattern, labels):: [
    {
      action: 'keep',
      sourceLabels: ['%s' % [label] for label in labels],
      regex: regexPattern,
    },
  ],
  // Drop series based on __name__ label
  dropSeriesByName(regexPattern):: [
    {
      action: 'drop',
      sourceLabels: ['__name__'],
      regex: regexPattern,
    },
  ],
  // Keep series based on __name__ label
  // WARNING: this will drop all other series.
  keepSeriesByName(regexPattern):: [
    {
      action: 'keep',
      sourceLabels: ['__name__'],
      regex: regexPattern,
    },
  ],
  // replace the address port with the passed one
  replacePort(port):: [
    {
      action: 'replace',
      regex: '(.+):\\d+',
      replacement: '${1}:' + port,
      sourceLabels: ['__address__'],
      targetLabel: '__address__',
    },
  ],

  // Default relabelings for all GCE prometheus jobs
  gceDefaults:: [
    // Map instance_name|machine_type|zone as labels
    {
      action: 'labelmap',
      regex: '__meta_gce_(instance_name|machine_type|zone)',
    },
    // Map all GCE labels as Prometheus labels
    {
      action: 'labelmap',
      regex: '__meta_gce_label_(.*)',
    },
    // Copy instance_name to instance
    {
      action: 'replace',
      regex: '([a-zA-Z0-9\\-]+)',
      sourceLabels: ['instance_name'],
      targetLabel: 'instance',
    },
    // Copy instance_name to fqdn
    // fqdn labels is added to be backwards compatible. However, we should
    // deprecated the use of the fqdn after the project is completed
    // TODO: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2744
    {
      action: 'replace',
      sourceLabels: ['__meta_gce_metadata_CHEF_NODE_NAME'],
      targetLabel: 'fqdn',
    },
    // Add the scrape port as a label
    {
      action: 'replace',
      regex: '.+:(\\d+)',
      replacement: '${1}',
      sourceLabels: ['__address__'],
      targetLabel: 'port',
    },
    // Extract the zone name from the GCE URL format
    {
      action: 'replace',
      regex: '^.*/zones/(.+)$',
      replacement: '$1',
      sourceLabels: ['zone'],
      targetLabel: 'zone',
    },
    // Extract the machineType from the GCE URL format
    {
      action: 'replace',
      regex: '^.*/machineTypes/(.+)$',
      replacement: '$1',
      sourceLabels: ['machine_type'],
      targetLabel: 'machine_type',
    },
    // Rename gitlab_com_service to service
    // TODO: drop this label when is not require anymore. Currently only use
    // for troubleshooting
    {
      action: 'replace',
      regex: '([a-zA-Z0-9\\-]+)',
      sourceLabels: ['gitlab_com_service'],
      targetLabel: 'service',
    },
    // Rename gitlab_com_type to type
    // It will replace whatever previous value type had, meanwhile the label
    // exist in GCE labels, when it doesn't exit it just add the label
    {
      action: 'replace',
      regex: '([a-zA-Z0-9\\-]+)',
      sourceLabels: ['gitlab_com_type'],
      targetLabel: 'type',
    },
    // Drop the label instance_name and any label that start with gl*
    // And drop labels staring with gitlab_com
    {
      action: 'labeldrop',
      regex: '(instance_name|pet_name|ansible_deploy_name|gl_.*|gitlab_com.*)',
    },
  ],

  // Drop any target that has the GCE tag gke-node
  // Effectively means all GKE nodes
  gceAllInstances:: [
    {
      action: 'drop',
      sourceLabels: ['__meta_gce_tags'],
      regex: '.*,gke-node,.*',
    },
  ],

  // blackbox exporter multi-target exporter pattern
  blackbox:: [
    {
      // __address__ contains the values from targets
      // this will add a parameter target to the prometheus scrape requests
      sourceLabels: ['__address__'],
      targetLabel: '__param_target',
    },
    {
      // copy the __param_target to the label instance
      // e.g. instance=https://gitlab.com
      sourceLabels: ['__param_target'],
      targetLabel: 'instance',
    },
    {
      sourceLabels: ['module'],
      targetLabel: '__param_module',
    },
    {
      // copy the value of __target_instance to the label __address__
      // __address__ will be used as the hostname and port for the prometheus
      // scrape requests. So that it queries the exporter and not the target
      // URI directly.
      // __target_instance should contain the hostname:port of the blackbox
      // exporter
      sourceLabels: ['__target_instance'],
      targetLabel: '__address__',
    },
  ],

  // windows runners replaces __address__ with public IP
  windowsRunners:: [
    {
      sourceLabels: ['__meta_gce_public_ip', '__meta_gce_label_shard', 'port'],
      regex: '(.*);(windows-shared);(.*)',
      targetLabel: '__address__',
      replacement: '${1}:${3}',
    },
  ],

  // sets a stage main label, even if it exists on source
  stageMain:: [
    {
      action: 'replace',
      targetLabel: 'stage',
      replacement: 'main',
    },
  ],
}
