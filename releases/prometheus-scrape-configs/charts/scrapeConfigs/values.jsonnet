local relabelings = import './relabelings.libsonnet';
local utils = import './utils.libsonnet';

{
  // Overrides the chart's name
  nameOverride: null,
  // Overrides the chart's computed fullname. Used to change the full prefix
  // of resources
  fullnameOverride: null,

  // Pod annotations for k8s manifests managed by this chart
  podAnnotations: {},

  // Pod labels for k8s manifests managed by this chart
  podLabels: {},

  // newScrapeConfig
  scrapeConfig:: {
    new(jobName)::
      self +
      self.mixin.withJobName(jobName),
    spec: {
      jobName: '',
      gceSDConfigs: [],
      staticConfigs: [],
      metricRelabelings: [],
      relabelings: [],
      scrapeInterval: '15s',
      scrapeTimeout: '10s',
    },
    mixin:: {
      local __specMixin(s) = { spec+: s },
      withJobName(jobName):: self + __specMixin({ jobName: jobName }),
      withRelabelings(relabelings):: self + __specMixin({ relabelings: relabelings }),
      withMetricRelabelings(metricRelabelings):: self + __specMixin({ metricRelabelings: metricRelabelings }),
      withScrapeInterval(scrapeInterval):: self + __specMixin({ scrapeInterval: scrapeInterval }),
      withScrapeTimeout(scrapeTimeout):: self + __specMixin({ scrapeTimeout: scrapeTimeout }),
      withMetricsPath(metricsPath):: self + __specMixin({ metricsPath: metricsPath }),
      withParams(params):: self + __specMixin({ params: params }),
    },
  },

  // Define the scrape Configs that will be generated
  scrapeConfigs: std.map(function(x) x.spec, [
    $.scrapeConfig.new('node-exporter-gce') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.gceAllInstances +
      relabelings.gceDefaults +
      relabelings.replacePort('9100') +
      [
        {
          action: 'replace',
          sourceLabels: ['stage'],
          targetLabel: 'stage',
          regex: '^$',
          replacement: 'main',
        },
      ]
    ) +
    $.scrapeConfig.mixin.withMetricRelabelings(
      [
        {
          action: 'replace',
          sourceLabels: ['__name__', 'release'],
          regex: 'node_uname_info;(.+)-(.+)-(.+)',
          targetLabel: 'base_version',
          replacement: '$1',
        },
        {
          action: 'replace',
          sourceLabels: ['__name__', 'release'],
          regex: 'node_uname_info;(.+)-(.+)-(.+)',
          targetLabel: 'abi_number',
          replacement: '$2',
        },
        {
          action: 'replace',
          sourceLabels: ['__name__', 'release'],
          regex: 'node_uname_info;(.+)-(.+)-(.+)',
          targetLabel: 'flavor',
          replacement: '$3',
        },
      ],
    ),

    // fluentd metrics
    $.scrapeConfig.new('fluentd') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.gceAllInstances +
      relabelings.gceDefaults +
      relabelings.replacePort('24231'),
    ),

    // epbf exporter
    $.scrapeConfig.new('ebpf') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.gceAllInstances +
      relabelings.gceDefaults +
      relabelings.replacePort('9435'),
    ),

    // cadivsor exporter
    $.scrapeConfig.new('cadvisor') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('gitaly', '(gitaly)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9101'),
    ) +
    $.scrapeConfig.mixin.withMetricRelabelings(
      [
        {
          action: 'keep',
          regex: '((cadvisor|go|machine|process)_.*|container_(cpu|memory).*;(/|/user.slice|/system.slice/gitlab.*|/gitaly.*))',
          sourceLabels: ['__name__', 'id'],
        },
        {
          action: 'drop',
          regex: 'container_memory_swap',
          sourceLabels: ['__name__'],
        },
      ],
    ) +
    $.scrapeConfig.mixin.withScrapeInterval('1m') +
    $.scrapeConfig.mixin.withScrapeTimeout('45s'),

    // haproxy metrics or haproxy exporter
    $.scrapeConfig.new('haproxy') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('haproxy', '(haproxy.*)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9101'),
    ) +
    $.scrapeConfig.mixin.withMetricRelabelings(
      [
        {
          action: 'replace',
          regex: 'haproxy_frontend.+;(.+)',
          sourceLabels: ['__name__', 'proxy'],
          targetLabel: 'frontend',
        },
        {
          action: 'replace',
          regex: 'haproxy_server.+;(.+)',
          sourceLabels: ['__name__', 'proxy'],
          targetLabel: 'backend',
        },
        {
          action: 'replace',
          regex: 'haproxy_backend.+;(.+)',
          sourceLabels: ['__name__', 'proxy'],
          targetLabel: 'backend',
        },
      ],
    ),

    // gitaly metrics
    $.scrapeConfig.new('gitaly') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('gitaly', '(gitaly)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9236'),
    ),

    // redis exporter
    $.scrapeConfig.new('redis') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('redis', '(redis)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9121'),
    ),

    // elasticsearch exporter
    $.scrapeConfig.new('elasticsearch') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('elasticsearch', '(sd-exporter|elasticsearch)') +
      relabelings.gceDefaults + [
        // Adjust label type based on the port
        {
          action: 'replace',
          sourceLabels: ['port'],
          regex: '(9114)',
          targetLabel: 'type',
          replacement: 'logging',
        },
        {
          action: 'replace',
          sourceLabels: ['port'],
          regex: '(9115)',
          targetLabel: 'type',
          replacement: 'search',
        },
      ],
    ) +
    $.scrapeConfig.mixin.withScrapeInterval('30s') +
    $.scrapeConfig.mixin.withScrapeTimeout('28s'),

    // pgbouncer exporter
    $.scrapeConfig.new('pgbouncer') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('pgbouncer', '(pgbouncer|postgres)') +
      relabelings.gceDefaults +
      relabelings.stageMain,
    ),

    // postgres exporter
    $.scrapeConfig.new('postgres') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('postgres', '(postgres)') +
      relabelings.gceDefaults +
      relabelings.stageMain +
      relabelings.replacePort('9187'),
    ) +
    $.scrapeConfig.mixin.withScrapeInterval('30s') +
    $.scrapeConfig.mixin.withScrapeTimeout('28s'),

    // postgres database bloat metrics
    $.scrapeConfig.new('postgres-database-bloat') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('postgres', '(postgres)') +
      relabelings.gceDefaults +
      relabelings.stageMain +
      relabelings.replacePort('9168'),
    ) +
    $.scrapeConfig.mixin.withMetricsPath('/database_bloat') +
    // TODO: review this scrape interval. See promethues Staleness.
    $.scrapeConfig.mixin.withScrapeInterval('60m') +
    $.scrapeConfig.mixin.withScrapeTimeout('2m'),

    // process exporter
    $.scrapeConfig.new('process-exporter') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.gceAllInstances +
      relabelings.gceDefaults +
      relabelings.replacePort('9256'),
    ),

    // gitaly praefect metrics
    $.scrapeConfig.new('praefect') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('praefect', '(praefect)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9652'),
    ),

    // gitlay praefect db metrics
    $.scrapeConfig.new('praefect-db') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('praefect', '(praefect)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9652'),
    ) +
    $.scrapeConfig.mixin.withMetricsPath('/db_metrics') +
    $.scrapeConfig.mixin.withScrapeInterval('60s') +
    $.scrapeConfig.mixin.withScrapeTimeout('30s'),

    // consul metrics
    $.scrapeConfig.new('consul') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.gceAllInstances +
      relabelings.gceDefaults +
      relabelings.replacePort('8500'),
    ) +
    $.scrapeConfig.mixin.withMetricRelabelings(
      relabelings.dropSeriesByName('(consul_fsm.*)'),
    ) +
    $.scrapeConfig.mixin.withMetricsPath('/v1/agent/metrics') +
    $.scrapeConfig.mixin.withParams({ format: ['prometheus'] }),

    // mailroom-inbox metrics
    $.scrapeConfig.new('mailroom-inbox') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('mailroom-inbox', '(sd-exporter)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9117'),
    ) +
    $.scrapeConfig.mixin.withScrapeInterval('120s') +
    $.scrapeConfig.mixin.withScrapeTimeout('30s'),

    // mtail metrics
    $.scrapeConfig.new('mtail') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.gceAllInstances +
      relabelings.gceDefaults +
      relabelings.replacePort('3903'),
    ),

    // pushgateway metrics
    $.scrapeConfig.new('pushgateway') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('pushgateway', '(postgres|blackbox)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9091'),
    ),

    // sidekiq-redis metrics
    $.scrapeConfig.new('sidekiq-redis') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByServiceAndType('sidekiq-redis', '(redis);(redis-sidekiq)') +
      relabelings.gceDefaults +
      relabelings.replacePort('4567'),
    ) +
    $.scrapeConfig.mixin.withMetricsPath('/sidekiq'),

    // stackdriver exporter
    $.scrapeConfig.new('stackdriver') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('stackdriver', '(sd-exporter)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9255'),
    ) +
    $.scrapeConfig.mixin.withScrapeInterval('60s') +
    $.scrapeConfig.mixin.withScrapeTimeout('50s') +
    $.scrapeConfig.mixin.withParams(
      { collect: [
        'cloudsql.googleapis.com/database/disk',
        'cloudsql.googleapis.com/database/instance_state',
        'cloudsql.googleapis.com/database/memory',
        'cloudsql.googleapis.com/database/mysql',
        'cloudsql.googleapis.com/database/postgresql/transaction_count',
        'cloudsql.googleapis.com/database/state',
        'cloudsql.googleapis.com/database/up',
        'compute.googleapis.com/firewall',
        'compute.googleapis.com/instance/disk/throttled_read_',
        'compute.googleapis.com/instance/disk/throttled_write_',
        'compute.googleapis.com/instance/integrity',
        'compute.googleapis.com/instance/uptime',  // Used to alert on GKE instance limit saturation.
        'compute.googleapis.com/mirroring',
        'compute.googleapis.com/nat',
        'container.googleapis.com',
        'file.googleapis.com',
        'loadbalancing.googleapis.com/https/backend_latencies',
        'loadbalancing.googleapis.com/https/backend_request_',
        'loadbalancing.googleapis.com/https/backend_response_',
        'loadbalancing.googleapis.com/https/request_',
        'loadbalancing.googleapis.com/https/response_',
        'loadbalancing.googleapis.com/l3/external/egress_',
        'loadbalancing.googleapis.com/l3/external/ingress_',
        'loadbalancing.googleapis.com/l3/internal/egress_',
        'loadbalancing.googleapis.com/l3/internal/ingress_',
        'logging.googleapis.com',
        'monitoring.googleapis.com',
        // Disabled the above metrics:
        // - Very expensive time wise, high number of series.
        // - High cardinality.
        // All details: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2710#note_1766827944
        // 'networking.googleapis.com/vm_flow/egress_bytes_count',
        // 'networking.googleapis.com/vm_flow/ingress_bytes_count',
        'pubsub.googleapis.com/topic/byte_cost',
        'pubsub.googleapis.com/topic/send_message_operation_count',
        'pubsub.googleapis.com/subscription',
        'router.googleapis.com/nat',
        'storage.googleapis.com',
        'vpn.googleapis.com',
      ] }
    ),

    // gitlab-exporter monitor database
    // https://gitlab.com/gitlab-org/ruby/gems/gitlab-exporter
    $.scrapeConfig.new('gitlab-monitor-database') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('gitlab-monitor-database', '(postgres)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9168'),
    ) +
    $.scrapeConfig.mixin.withMetricsPath('/database'),

    // gitlab-exporter monitor database
    // https://gitlab.com/gitlab-org/ruby/gems/gitlab-exporter
    // gitlab_database_rows
    $.scrapeConfig.new('gitlab-monitor-database-mirroring') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByServiceAndType('gitlab-monitor-database', '(postgres);(patroni|patroni-registry)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9168'),
    ) +
    $.scrapeConfig.mixin.withScrapeInterval('30s') +
    $.scrapeConfig.mixin.withMetricsPath('/mirroring'),

    // blackbox exporter metrics
    $.scrapeConfig.new('blackbox-exporter') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('blackbox', '(blackbox)') +
      relabelings.gceDefaults +
      relabelings.replacePort('9115'),
    ),

    // blackbox probe targets
    $.scrapeConfig.new('blackbox') +
    $.scrapeConfig.mixin.withMetricsPath('/probe') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.blackbox,
    ),
    // ci-runners probe targets
    $.scrapeConfig.new('runner') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByService('runner', '(runner)') +
      relabelings.gceDefaults +
      // This must go any other __address__ relabel config
      relabelings.windowsRunners +
      relabelings.stageMain +
      relabelings.replacePort('9402'),
    ),

    $.scrapeConfig.new('wmi') +
    $.scrapeConfig.mixin.withRelabelings(
      relabelings.filterByCustomLabels('runner;windows-shared', ['__meta_gce_label_gitlab_com_service', '__meta_gce_label_shard']) +
      relabelings.gceDefaults +
      // This must go any other __address__ relabel config
      relabelings.windowsRunners +
      relabelings.stageMain +
      relabelings.replacePort('9182'),
    ),
  ]),

  // All gceSDConfigs, staticConfigs are a empty list by default, so they can
  // be easy override per environment. Sadly, helm charts don't support a easy
  // way to merge lists
  customConfigs: {
    gceSDConfigs: {
      [utils.camelCase(j.jobName)]: []
      for j in $.scrapeConfigs
    },
    staticConfigs: {
      [utils.camelCase(j.jobName)]: []
      for j in $.scrapeConfigs
    },
  },

  // All scrapeConfig jobs are enabled by default
  JobEnabled: {
    [utils.camelCase(j.jobName)]: true
    for j in $.scrapeConfigs
  },
}
