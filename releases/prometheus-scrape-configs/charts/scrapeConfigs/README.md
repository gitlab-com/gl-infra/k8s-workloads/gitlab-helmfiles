# prometheus-agent

## Introduction

Starting with Prometheus-operator v0.65.x, one can use the ScrapeConfig CRD to
scrape targets external to the Kubernetes cluster or create scrape
configurations that are not possible with the higher-level
ServiceMonitor/Probe/PodMonitor resources.

ScrapeConfig currently supports a limited set of service discoveries:
static_config, file_sd, http_sd, kubernets_sd, consul_sd, gce_sd_config.

However, this Helm chart only implements the ones that are required so far:
`gce_sd_config` and `static_configs`:

- `gce_sd_config`: GCE SD configurations allow retrieving scrape targets from GCP
  GCE instances.

- `static_config`: Allows specifying a list of targets and a standard label set for
  them.

## Chart design principles

### Goals

- We want a common set of Prometheus jobs with a standard configuration that
  can be used to scrape across all the different exporters we have in our VM
  fleet.

- We avoid writing complex configurations per environment. We only want to turn
  on/off a job and the minimum configuration required to make that environment
  work.

- To force the standardization across environments, it won't be possible to
  customize `relabelings` or `metrics_relabelings` per environment.

We used jsonnet to generate the values.yaml, to avoid code repetition and
simplify maintenance.

### Notes

Prometheus configuration is mostly yaml dictionaries and lists. In most cases,
we must merge lists, which is only possible in the helm template system with
complex caveats. That is where `jsonnet` does a much better job, as merging any
object or array is possible.

## ScrapeConfig Jobs

### node-exporter-gce

Job for node-exporter using discovery via the GCE API

Default port for node-exporter: 9100

### fluentd

Job for fluentd metrics using GCE API for discovery

Default port for fluentd metrics: 24231

### ebpf

Job for ebpf metrics using GCE API for discovery

Default port for ebpf metrics: 9435

### Cadvisor

Job for cadvisor metrics using the GCE API for discovery

Default port for cadvisor metrics: 9101

### haproxy

Job for haproxy metrics using the GCE API for discovery

Default port for haproxy metrics: 9101

### blackbox exporter

The blackbox exporter allows blackbox probing of endpoints over HTTP, HTTPS,
DNS, TCP, ICMP and gRPC.

A stateless go process runs in the same and different infrastructure to probe
internal and external targets over several protocols. This exporter generates
multiple metrics on the configured target line general endpoint status
(latency, reachability, endpoint failures), response time, redirect
information, certification expiration dates

Given the limitations of helm charts, we will probably migrate the blackbox
exporter away from running in GCE instances in another region. We've
implemented a compromise solution that allows the definition of different
targets per environment.

Mainly, this solution avoids the need to create many `scrapeConfigs` per
blackbox exporter module, per different scrape interval, or per different
static labels.

Here an example of the yaml that needs to be defined per environment:

```yaml
customConfigs:
  staticConfigs:
    Blackbox:
      - targets:
         - google.com
         - gitlab.com
        labels:
          env: gstg
          shard: default
          tier: sv
          type: blackbox
          __param_module: 'http_2xx'
          __scrape_interval__: 30s
          __scrape_timeout__: 28s
          __target_instance: blackbox-exporter-uri
      - targets:
         - gitlab.com:22
        labels:
          type: blackbox
          __param_module: 'ssh_banner'
          __scrape_interval__: 5s
          __scrape_timeout__: 4s
          __target_instance: blackbox-exporter-uri

```

In the above yaml, it's setup the following:

- Using the blackbox exporter module `http_2xx`, every 30s against the
  `__target_instance` to scrape the targets: google.com, gitlab.com
- Using the blackbox exporter module `ssh_baner`, every 5s against the
  `__target_instance` to scrape the target gitlab.com:22

In every case, we also adding the relevant static labels for those targets.
