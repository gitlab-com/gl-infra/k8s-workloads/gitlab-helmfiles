{
  local c = (import './config.libsonnet'),
  local gitalyGstg = {
    'gitlab-gitaly-gstg-164c': c.zones.default,
    'gitlab-gitaly-gstg-380a': c.zones.default,
  },

  // Enabled scrape Jobs
  JobEnabled: {
    Runner: false,
    Stackdriver: false,
    Wmi: false,
  },

  customConfigs: {
    // Add scrapeConfigs with their relevant projects and zones
    gceSDConfigs:
      c.gceSDConfig(
        'NodeExporterGce',
        {
          'gitlab-staging-1': c.zones.all,
        } +
        gitalyGstg,
      ) +
      c.gceSDConfig(
        'Fluentd',
        {
          'gitlab-staging-1': c.zones.all,
        } +
        gitalyGstg,
      ) +
      c.gceSDConfig(
        'Ebpf',
        {
          'gitlab-staging-1': c.zones.all,
        } +
        gitalyGstg,
      ) +
      c.gceSDConfig(
        'Cadvisor',
        {
          'gitlab-staging-1': c.zones.default,
        } +
        gitalyGstg,
      ) +
      c.gceSDConfig(
        'Haproxy',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'Gitaly',
        {
          'gitlab-staging-1': c.zones.default,
        } +
        gitalyGstg,
      ) +
      c.gceSDConfig(
        'Redis',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      // Elasticsearch and Pgbouncer don't have their ports defined by the underlying Chart
      // as they're using different ports for the same project and zone combination.
      {
        Elasticsearch: [
          {
            project: 'gitlab-staging-1',
            zone: 'us-central1-b',
            port: 9114,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-central1-b',
            port: 9115,
          },
        ],
        Pgbouncer: [
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-b',
            port: 9188,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-c',
            port: 9188,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-d',
            port: 9188,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-b',
            port: 9189,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-c',
            port: 9189,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-d',
            port: 9189,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-b',
            port: 9190,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-c',
            port: 9190,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-d',
            port: 9190,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-b',
            port: 9191,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-c',
            port: 9191,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-d',
            port: 9191,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-b',
            port: 9192,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-c',
            port: 9192,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-d',
            port: 9192,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-b',
            port: 9193,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-c',
            port: 9193,
          },
          {
            project: 'gitlab-staging-1',
            zone: 'us-east1-d',
            port: 9193,
          },
        ],
      } +
      c.gceSDConfig(
        'Postgres',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'PostgresDatabaseBloat',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'ProcessExporter',
        {
          'gitlab-staging-1': c.zones.all,
        } +
        gitalyGstg,
      ) +
      c.gceSDConfig(
        'Praefect',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'PraefectDb',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'Consul',
        {
          'gitlab-staging-1': c.zones.all,
        } +
        gitalyGstg,
      ) +
      c.gceSDConfig(
        'MailroomInbox',
        {
          'gitlab-staging-1': ['us-central1-b'],
        },
      ) +
      c.gceSDConfig(
        'Mtail',
        {
          'gitlab-staging-1': c.zones.all,
        } +
        gitalyGstg,
      ) +
      c.gceSDConfig(
        'Pushgateway',
        {
          'gitlab-staging-1': c.zones.all,
        },
      ) +
      c.gceSDConfig(
        'SidekiqRedis',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'GitlabMonitorDatabase',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'GitlabMonitorDatabaseMirroring',
        {
          'gitlab-staging-1': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'BlackboxExporter',
        {
          'gitlab-staging-1': ['us-central1-b'],
        },
      ),

    staticConfigs: {
      Blackbox: [
        {
          targets: [
            'https://gitlab-examples.gitlab.io',
            'https://gitlab.com/gitlab-com/gl-infra/production-engineering/raw/main/alert-test',
            'http://staging.pages-check.gitlab.net',
            'https://staging.gitlab.com/users/sign_in',
            'https://staging.pages-check.gitlab.net',
            'https://user-content.staging.gitlab-static.net/healthcheck',
            'https://registry.gitlab.com',
            'https://internal-gateway.gstg.gitlab.net:11443/users/sign_in',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_2xx',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gstg.c.gitlab-staging-1.internal:9115',
          },
        },
        {
          targets: [
            'staging.gitlab.com:22',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'ssh_banner',
            __scrape_interval__: '5s',
            __scrape_timeout__: '4s',
            __target_instance: 'blackbox-01-inf-gstg.c.gitlab-staging-1.internal:9115',
          },
        },
        {
          targets: [
            'https://e8fabf53b73247e98df7c355d6a782fc.us-central1.gcp.cloud.es.io:9243/*/_ilm/explain',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_elastic_nonprod_ilm',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gstg.c.gitlab-staging-1.internal:9115',
          },
        },
        {
          targets: [
            'https://e8fabf53b73247e98df7c355d6a782fc.us-central1.gcp.cloud.es.io:9243/_ilm/status',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_elastic_nonprod_ilm_status',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gstg.c.gitlab-staging-1.internal:9115',
          },
        },
        {
          targets: [
            'https://prometheus.gstg.gitlab.net',
            'https://prometheus-app.gstg.gitlab.net',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'https_redirect',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gstg.c.gitlab-staging-1.internal:9115',
          },
        },
      ],
    },
  },
}
