{
  local c = (import './config.libsonnet'),
  local gitalyGprd = {
    'gitlab-gitaly-gprd-0fe1': c.zones.default,
    'gitlab-gitaly-gprd-256e': c.zones.default,
    'gitlab-gitaly-gprd-2e35': c.zones.default,
    'gitlab-gitaly-gprd-5a25': c.zones.default,
    'gitlab-gitaly-gprd-6688': c.zones.default,
    'gitlab-gitaly-gprd-7ebd': c.zones.default,
    'gitlab-gitaly-gprd-83fd': c.zones.default,
    'gitlab-gitaly-gprd-87a9': c.zones.default,
    'gitlab-gitaly-gprd-93cb': c.zones.default,
    'gitlab-gitaly-gprd-a606': c.zones.default,
    'gitlab-gitaly-gprd-ccb0': c.zones.default,
    'gitlab-gitaly-gprd-cdaf': c.zones.default,
    'gitlab-gitaly-gprd-d1a2': c.zones.default,
    'gitlab-gitaly-gprd-e493': c.zones.default,
    'gitlab-gitaly-gprd-f33d': c.zones.default,
  },

  JobEnabled: {
    NodeExporterGce: true,
    Fluentd: true,
    Ebpf: true,
    Cadvisor: true,
    Haproxy: true,
    Gitaly: true,
    Redis: true,
    Elasticsearch: true,
    Pgbouncer: true,
    Postgres: true,
    PostgresDatabaseBloat: true,
    ProcessExporter: true,
    Praefect: true,
    PraefectDb: true,
    Consul: true,
    MailroomInbox: true,
    Mtail: true,
    Pushgateway: true,
    SidekiqRedis: true,
    Stackdriver: false,
    GitlabMonitorDatabase: true,
    GitlabMonitorDatabaseMirroring: true,
    BlackboxExporter: true,
    Blackbox: true,
    Runner: true,
  },

  customConfigs: {
    // Add scrapeConfigs with their relevant projects and zones
    gceSDConfigs:
      c.gceSDConfig(
        'NodeExporterGce',
        {
          'gitlab-production': c.zones.all,
          'gitlab-ci-155816': c.zones.default,
          'gitlab-qa-runners-2': c.zones.default,
        } +
        gitalyGprd,
      ) +
      c.gceSDConfig(
        'Fluentd',
        {
          'gitlab-production': c.zones.all,
        } +
        gitalyGprd,
      ) +
      c.gceSDConfig(
        'Ebpf',
        {
          'gitlab-production': c.zones.all,
        } +
        gitalyGprd,
      ) +
      c.gceSDConfig(
        'Cadvisor',
        {
          'gitlab-production': c.zones.default,
        } +
        gitalyGprd,
      ) +
      c.gceSDConfig(
        'Haproxy',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'Gitaly',
        {
          'gitlab-production': c.zones.default,
        } +
        gitalyGprd,
      ) +
      c.gceSDConfig(
        'Redis',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      // Elasticsearch and Pgbouncer don't have their ports defined by the underlying Chart
      // as they're using different ports for the same project and zone combination.
      {
        Elasticsearch: [
          {
            project: 'gitlab-production',
            zone: 'us-central1-b',
            port: 9114,
          },
          {
            project: 'gitlab-production',
            zone: 'us-central1-b',
            port: 9115,
          },
        ],
        Pgbouncer: [
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9188,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9188,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9188,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9189,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9189,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9189,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9190,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9190,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9190,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9191,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9191,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9191,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9192,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9192,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9192,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9193,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9193,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9193,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9194,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9194,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9194,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9195,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9195,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9195,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9196,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9196,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9196,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-b',
            port: 9197,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-c',
            port: 9197,
          },
          {
            project: 'gitlab-production',
            zone: 'us-east1-d',
            port: 9197,
          },
        ],
      } +
      c.gceSDConfig(
        'Postgres',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'PostgresDatabaseBloat',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'ProcessExporter',
        {
          'gitlab-production': c.zones.all,
          'gitlab-ci-155816': c.zones.default,
          'gitlab-qa-runners-2': c.zones.default,
        } +
        gitalyGprd,
      ) +
      c.gceSDConfig(
        'Praefect',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'PraefectDb',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'Consul',
        {
          'gitlab-production': c.zones.all,
        } +
        gitalyGprd,
      ) +
      c.gceSDConfig(
        'MailroomInbox',
        {
          'gitlab-production': ['us-central1-b'],
        },
      ) +
      c.gceSDConfig(
        'Mtail',
        {
          'gitlab-production': c.zones.all,
        } +
        gitalyGprd,
      ) +
      c.gceSDConfig(
        'Pushgateway',
        {
          'gitlab-production': c.zones.all,
        },
      ) +
      c.gceSDConfig(
        'SidekiqRedis',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'GitlabMonitorDatabase',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'GitlabMonitorDatabaseMirroring',
        {
          'gitlab-production': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'BlackboxExporter',
        {
          'gitlab-production': ['us-central1-b'],
        },
      ) +
      c.gceSDConfig(
        'Runner',
        {
          'gitlab-ci-windows': c.zones.default,
          'gitlab-ci-155816': c.zones.default,
          'gitlab-qa-runners-2': c.zones.default,
        },
      ) +
      c.gceSDConfig(
        'Wmi',
        {
          'gitlab-ci-windows': c.zones.default,
        },
      ),

    staticConfigs: {
      Blackbox: [
        {
          targets: [
            'http://prod.pages-check.gitlab.net',
            'https://design.gitlab.com',
            'https://about.gitlab.com',
            'https://about.gitlab.com/blog/',
            'https://about.gitlab.com/releases/',
            'https://about.gitlab.com/releases/categories/releases/',
            'https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/',
            'https://about.gitlab.com/direction/',
            'https://about.gitlab.com/company/team/',
            'https://about.gitlab.com/handbook/',
            'https://about.gitlab.com/handbook/values/',
            'https://handbook.gitlab.com/handbook/values/',
            'https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/calculator/',
            'https://about.gitlab.com/handbook/engineering/projects/',
            'https://chef.gitlab.com/favicon.ico',
            'https://customers.gitlab.com/-/liveness/database,migrations,cache',
            'https://dashboards.gitlab.net',
            'https://dev.gitlab.org',
            'https://dev.gitlab.org:5005',
            'https://docs.gitlab.com',
            'https://gitlab-examples.gitlab.io',
            'https://gitlab.com/explore',
            'https://gitlab.com/explore/projects/starred',
            'https://gitlab.com/gitlab-com/gitlab-com-infrastructure/tree/master"',
            'https://gitlab.com/gitlab-com/gl-infra/production-engineering/raw/main/alert-test',
            'https://gitlab.com/gitlab-org/gitlab-foss/',
            'https://gitlab.com/gitlab-org/gitlab-foss/issues',
            'https://gitlab.com/gitlab-org/gitlab-foss/-/issues/1',
            'https://gitlab.com/gitlab-org/gitlab-foss/merge_requests/',
            'https://gitlab.com/gitlab-org/gitlab-foss/tree/master',
            'https://gitlab.com/sytses/test-2/issues/1',
            'https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1',
            'https://internal-gateway.gprd.gitlab.net:11443/users/sign_in',
            'https://ops.gitlab.net/users/sign_in',
            'https://packages.gitlab.com',
            'https://pages.gitlab.io',
            'https://prod.pages-check.gitlab.net',
            'https://registry.gitlab.com',
            'https://release.gitlab.net',
            'https://new-sentry.gitlab.net',
            'https://staging.gitlab.com/gitlab-com/operations/issues/42',
            'https://staging.gitlab.com/users/sign_in',
            'https://status.gitlab.com',
            'https://user-content.gitlab-static.net/healthcheck',
            'https://version.gitlab.com',
            'https://next.gitlab.com',
            'https://registry.ops.gitlab.net',
            'https://plantuml.gitlab-static.net/png/',
            'https://plantuml.gitlab-static.net/png/U9mBoapETYmkoKcjLD2rKuZCBrMmKl1KprN8IorBBL98py_BIrH8ponBLQW4IfKMA78109Vb42O0',
            'https://pre.plantuml.gitlab-static.net/png/',
            'https://gstg.plantuml.gitlab-static.net/png/',
            'https://pre.gitlab.com',
            'https://registry.pre.gitlab.com',
            'https://cdn.registry.staging.gitlab-static.net/cdn-test/health?Expires=2274756359&KeyName=gstg-registry-cdn&Signature=udcmVD9Zz8g5sJP5dLwv6EHr1GY=',
            'https://cdn.registry.gitlab-static.net/cdn-test/health?Expires=2274756396&KeyName=gprd-registry-cdn&Signature=LPal0kx0X6KbIYrZ0Fzv8ptrHNM=',
            'https://cdn.artifacts.gitlab-static.net/cdn-test/health?Expires=2297220294&KeyName=gprd-artifacts-cdn&Signature=kK1PF00S_iOMaFMQUBp_JW9ICNw=',
            'https://cdn.artifacts.staging.gitlab-static.net/cdn-test/health?Expires=2297220458&KeyName=gstg-artifacts-cdn&Signature=y_hYjphBlN_EcAQBOqHPhIqRMRE=',
            'https://advisories.gitlab.com',
            'https://advisories.gitlab.com/pkg/golang/github.com/pocketbase/pocketbase/CVE-2024-38351/', // https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18374
            // Creating the below targets temporarily to monitor the worker, as part of: https://gitlab.com/gitlab-org/cells/http-router/-/issues/84
            'https://gitlab.com/ayufan',
            'https://gitlab.com/tkhandelwal3',
            'https://gitlab.com/gitlab-org/tenant-scale-group/test-cloudflare-worker-in-production'
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_2xx',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'staging.gitlab.com:22',
            'altssh.gitlab.com:443',
            'dev.gitlab.org:22',
            'gitlab.com:22',
            'ops.gitlab.net:22',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'ssh_banner',
            __scrape_interval__: '5s',
            __scrape_timeout__: '4s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://gitlab.com/explore',
            'https://gitlab.com/sytses/test-2/issues/1',
            'https://gitlab.com',
            'https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-foss',
            'https://gitlab.com/gitlab-com/infrastructure/issues',
            'https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/57',
            'https://gitlab.com/gitlab-com/www-gitlab-com',
            'https://gitlab.com/sytses/test-2/issues/1',
            'https://staging.gitlab.com/gitlab-com/operations/issues/42',
            'https://gitlab.com/explore',
            'https://gitlab.com/explore/projects/starred',
            // Creating the below targets temporarily to monitor the worker, as part of: https://gitlab.com/gitlab-org/cells/http-router/-/issues/84
            'https://gitlab.com/api/v4/users/210928',
            'https://gitlab.com/api/v4/users/21085035',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_gitlab_com_auth_2xx',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://alerts.gitlab.net',
            'https://blog.gitlab.com',
            'https://contributors.gitlab.com',
            'https://gitlab.com',
            'https://gitlab.com/projects/new',
            'https://prometheus-app.gprd.gitlab.net',
            'https://prometheus.gprd.gitlab.net',
            'https://prometheus.ops.gitlab.net',
            'https://staging.gitlab.com',
            'https://thanos-query.ops.gitlab.net',
            'https://www.gitlab.com',
            'https://www.remoteonly.org',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'https_redirect',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'http://about.gitlab.com',
            'http://blog.gitlab.com',
            'http://chef.gitlab.com',
            'http://contributors.gitlab.com',
            'http://customers.gitlab.com',
            'http://dashboards.gitlab.net',
            'http://dev.gitlab.org',
            'http://doc.gitlab.com',
            'http://docs.gitlab.com',
            'http://gitlab.com',
            'http://jobs.gitlab.com',
            'http://packages.gitlab.com',
            'http://registry.gitlab.com',
            'http://new-sentry.gitlab.net',
            'http://staging.gitlab.com',
            'http://status.gitlab.com',
            'http://version.gitlab.com',
            'http://next.gitlab.com',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'https_tls_upgrade',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://kas.gitlab.com/',
            'https://kas.staging.gitlab.com/',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_426',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'about.gitlab.com:443',
            'alerts.gitlab.net:443',
            'blog.gitlab.com:443',
            'chef.gitlab.com:443',
            'contributors.gitlab.com:443',
            'customers.gitlab.com:443',
            'dashboards.gitlab.net:443',
            'design.gitlab.com:443',
            'dev.gitlab.org:443',
            'docs.gitlab.com:443',
            'github.com:443',
            'gitlab-examples.gitlab.io:443',
            'gitlab.com:443',
            'gstg.plantuml.gitlab-static.net:443',
            'internal-gateway.gprd.gitlab.net:11443',
            'links.gitlab.com:443',
            'kas.gitlab.com:443',
            'kas.staging.gitlab.com:443',
            'next.gitlab.com:443',
            'ops.gitlab.net:443',
            'packages.gitlab.com:443',
            'pages.gitlab.io:443',
            'plantuml.gitlab-static.net:443',
            'pre.gitlab.com:443',
            'pre.plantuml.gitlab-static.net:443',
            'prod.pages-check.gitlab.net:443',
            'prometheus-app.gprd.gitlab.net:443',
            'prometheus.gprd.gitlab.net:443',
            'prometheus.ops.gitlab.net:443',
            'registry.gitlab.com:443',
            'registry.ops.gitlab.net:443',
            'registry.pre.gitlab.com:443',
            'release.gitlab.net:443',
            'new-sentry.gitlab.net:443',
            'staging.gitlab.com:443',
            'tls-test.staging.gitlab.io:443',
            'thanos-query.ops.gitlab.net:443',
            'version.gitlab.com:443',
            'www.gitlab.com:443',
            'cdn.registry.staging.gitlab-static.net:443',
            'cdn.registry.gitlab-static.net:443',
            'cdn.artifacts.staging.gitlab-static.net:443',
            'cdn.artifacts.gitlab-static.net:443',
            'cert-check.ci-gateway.int.gstg.gitlab.net:443',
            'cert-check.ci-gateway.int.gprd.gitlab.net:443',
            'status.gitlab.com:443',
            'ir.gitlab.com:443',
            'workspaces.gitlab.dev:443',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'tls_expiry',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://dev.gitlab.org/gitlab/gitlab-ee/merge_requests/5/diffs',
            'https://dev.gitlab.org/gitlab/gitlab-ee/issues',
            'https://dev.gitlab.org/gitlab/gitlabhq/issues/2677',
            'https://dev.gitlab.org/gitlab/support-middleware/issues/36',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_dev_gitlab_org_2xx',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://storage.googleapis.com/gitlab-ci-git-repo-cache/project-278964/gitlab-master-shallow.tar.gz',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_head_2xx',
            __scrape_interval__: '30s',
            __scrape_timeout__: '28s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://92c87c26b16049b0a30af16b94105528.us-central1.gcp.cloud.es.io:9243/*/_ilm/explain',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_elastic_prod_ilm',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://92c87c26b16049b0a30af16b94105528.us-central1.gcp.cloud.es.io:9243/_ilm/status',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_elastic_prod_ilm_status',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://92c87c26b16049b0a30af16b94105528.us-central1.gcp.cloud.es.io:9243/_cluster/health',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_elastic_prod_health',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://0ea4e34a81444a95a1adeb1f90ed9dfa.us-central1.gcp.cloud.es.io:9243/*/_ilm/explain',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_elastic_monitoring_es7_ilm',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://0ea4e34a81444a95a1adeb1f90ed9dfa.us-central1.gcp.cloud.es.io:9243/_ilm/status',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_elastic_monitoring_es7_ilm_status',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
        {
          targets: [
            'https://0ea4e34a81444a95a1adeb1f90ed9dfa.us-central1.gcp.cloud.es.io:9243/_cluster/health',
          ],
          labels: {
            stage: 'main',
            shard: 'default',
            tier: 'sv',
            type: 'blackbox',
            module: 'http_elastic_monitoring_es7_health',
            __scrape_interval__: '60s',
            __scrape_timeout__: '58s',
            __target_instance: 'blackbox-01-inf-gprd.c.gitlab-production.internal:9115',
          },
        },
      ],
    },
  },
}
