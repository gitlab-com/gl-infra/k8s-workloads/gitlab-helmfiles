{
  // Google Cloud zones by default
  zones: {
    default: ['us-east1-b', 'us-east1-c', 'us-east1-d'],
    central: ['us-central1-b'],  // we don't have GCE instances in other zones
    all: self.default + self.central,
  },

  // All scrapeJobs disabled
  scrapeJobs: {
    NodeExporterGCE: false,
    Fluentd: false,
    Ebpf: false,
    Cadvisor: false,
    Haproxy: false,
    Gitaly: false,
    Redis: false,
    Elasticsearch: false,
    Pgbouncer: false,
    Postgres: false,
    PostgresDatabaseBloat: false,
    ProcessExporter: false,
    Praefect: false,
    PraefectDb: false,
    Consul: false,
    MailroomInbox: false,
    Mtail: false,
    Pushgateway: false,
    SidekiqRedis: false,
    Stackdriver: false,
    GitlabMonitorDatabase: false,
    GitlabMonitorDatabaseMirroring: false,
    BlackboxExporter: false,
    Blackbox: false,
  },

  gceSDConfig(name, projectZoneMap): {
    [name]: [
      {
        project: project,
        zone: zone,
      }
      for project in std.objectFields(projectZoneMap)
      for zone in projectZoneMap[project]
    ],
  },
}
