{
  local c = (import './config.libsonnet'),

  JobEnabled: {
    Blackbox: false,
    BlackboxExporter: false,
    Cadvisor: false,
    Consul: false,
    Elasticsearch: false,
    Fluentd: false,
    Gitaly: false,
    Haproxy: false,
    MailroomInbox: false,
    Praefect: false,
    PraefectDb: false,
    Pushgateway: false,
    Redis: false,
    Runner: false,
    SidekiqRedis: false,
    Wmi: false,
  },

  customConfigs: {
    // Add scrapeConfigs with their relevant projects and zones
    gceSDConfigs:
      c.gceSDConfig('Ebpf', { 'gitlab-db-benchmarking': c.zones.default }) +
      c.gceSDConfig('Mtail', { 'gitlab-db-benchmarking': c.zones.default }) +
      c.gceSDConfig('NodeExporterGce', { 'gitlab-db-benchmarking': c.zones.default }) +
      // Pgbouncer doesn't have it's ports defined by the underlying Chart as it's
      // using different ports for the same project and zone combination.
      {
        Pgbouncer: [
          {
            project: 'gitlab-db-benchmarking',
            zone: 'us-east1-b',
            port: 9188,
          },
          {
            project: 'gitlab-db-benchmarking',
            zone: 'us-east1-c',
            port: 9188,
          },
          {
            project: 'gitlab-db-benchmarking',
            zone: 'us-east1-d',
            port: 9188,
          },
        ],
      } +
      c.gceSDConfig('Postgres', { 'gitlab-db-benchmarking': c.zones.default }) +
      c.gceSDConfig('PostgresDatabaseBloat', { 'gitlab-db-benchmarking': c.zones.default }) +
      c.gceSDConfig('ProcessExporter', { 'gitlab-db-benchmarking': c.zones.default }) +
      c.gceSDConfig('GitlabMonitorDatabase', { 'gitlab-db-benchmarking': c.zones.default }) +
      c.gceSDConfig('GitlabMonitorDatabaseMirroring', { 'gitlab-db-benchmarking': c.zones.default }),
  },
}
