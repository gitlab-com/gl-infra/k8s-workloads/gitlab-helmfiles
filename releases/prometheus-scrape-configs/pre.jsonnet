{
  local c = (import './config.libsonnet'),

  // Enabled scrape Jobs
  JobEnabled: c.scrapeJobs {
    NodeExporterGCE: true,
    Fluentd: true,
    Mtail: true,
    Haproxy: true,
    Gitaly: true,
    Redis: true,
    Praefect: true,
    ProcessExporter: true,
    Stackdriver: false,
    Runner: false,
    Wmi: false,
  },

  customConfigs: {
    // Add scrapeConfigs with their relevant projects and zones
    gceSDConfigs:
      c.gceSDConfig('NodeExporterGce', { 'gitlab-pre': c.zones.all }) +
      c.gceSDConfig('Fluentd', { 'gitlab-pre': c.zones.all }) +
      c.gceSDConfig('Mtail', { 'gitlab-pre': c.zones.all }) +
      c.gceSDConfig('Haproxy', { 'gitlab-pre': c.zones.default }) +
      c.gceSDConfig('Gitaly', { 'gitlab-pre': c.zones.default }) +
      c.gceSDConfig('Gitaly', { 'gitlab-pre': c.zones.default }) +
      c.gceSDConfig('Redis', { 'gitlab-pre': c.zones.default }) +
      c.gceSDConfig('Praefect', { 'gitlab-pre': c.zones.default }) +
      c.gceSDConfig('ProcessExporter', { 'gitlab-pre': c.zones.all }),
  },
}
