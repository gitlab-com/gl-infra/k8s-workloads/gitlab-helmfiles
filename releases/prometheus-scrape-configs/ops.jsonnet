{
  local c = (import './config.libsonnet'),

  // Enabled scrape Jobs
  JobEnabled: c.scrapeJobs {
    NodeExporterGCE: true,
    Fluentd: true,
    Mtail: true,
    Pushgateway: true,
    ProcessExporter: true,
    Stackdriver: false,
    Runner: false,
    Wmi: false,
  },

  customConfigs: {
    // Add scrapeConfigs with their relevant projects, zones and port
    gceSDConfigs:
      c.gceSDConfig('NodeExporterGce', { 'gitlab-ops': c.zones.all, 'gitlab-dev-1': ['us-east1-c'] }) +
      c.gceSDConfig('Fluentd', { 'gitlab-ops': c.zones.all }) +
      c.gceSDConfig('Mtail', { 'gitlab-ops': c.zones.all }) +
      c.gceSDConfig('Pushgateway', { 'gitlab-ops': ['us-east1-c'] }) +
      c.gceSDConfig('ProcessExporter', { 'gitlab-ops': c.zones.all }),
  },
}
