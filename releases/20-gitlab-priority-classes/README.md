# Priority Classes

Reference: https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/

In Kubernetes versions below 1.17, we cannot use the system default classes
outside of the `kube-system` namespace.  In here we'll define classes such that
we can ensure that workloads that MUST run will be prioritized.  Our
`fluentd-elasticsearch` pods are prime examples.
