---
# ref: https://github.com/vectordotdev/helm-charts/blob/develop/charts/vector/values.yaml
fullnameOverride: vector-agent

role: "Agent"

env:
  - name: CLICKHOUSE_USERNAME
    valueFrom:
      secretKeyRef:
        name: clickhouse-credentials
        key: username
  - name: CLICKHOUSE_PASSWORD
    valueFrom:
      secretKeyRef:
        name: clickhouse-credentials
        key: password

podMonitor:
  enabled: true
  relabelings:
    - action: replace
      replacement: vector
      targetLabel: type
    - action: replace
      replacement: main
      targetLabel: stage

## START VECTOR CONFIG ##

customConfig:
  data_dir: /vector-data-dir
  api:
    enabled: false
    address: 127.0.0.1:8686
    playground: false
  sources:
    kubernetes_logs:
      type: kubernetes_logs
      extra_namespace_label_selector: "kubernetes.io/metadata.name=gitlab"
    internal_metrics:
      type: internal_metrics
  transforms:
    clickhouse_container_logs:
      type: remap
      inputs:
        - kubernetes_logs
      source: |
        message_string = .message

        message =
            parse_syslog(.message) ??
            parse_nginx_log(string!(.message), "ingress_upstreaminfo") ??
            parse_common_log(.message) ??
            parse_json!(.message)

        .message = message

        del(.message.params)

        # params = []

        # if .kubernetes.namespace_labels.name == "gitlab" && exists(.message.params) {
        #     for_each(array!(.message.params)) -> |_index, value| {
        #         for_each([value]) -> |_, v| {
        #             params = append(params, [join!([v.key, v.value], "=")])
        #         }
        #     }
        #     .message.params = join!(params, "&")
        # }

        .kubernetes.cluster_name = "{{ .Values.cluster }}"

        # This isn't consistently set but is fine for this PoC
        .servicename = .kubernetes.pod_labels."app" || "unkonwn"

        .severitytext = .message.level || .message.severity || "unknown"

        .environment = "gstg"

        .messagetext = message_string

        . = merge!(., .message)

        del(.message)

        # Apply the recursive flatten function on the root event
        . = flatten(., "_")

        # Remove dots from keys
        . = map_keys(., recursive: true) -> |key| { replace(key, ".", "_") }

        # Delete unwanted log fields
        del(.file)
        del(.stream)
        del(.source_type)
        # Return final object
        .

  sinks:
    prom_exporter:
      type: prometheus_exporter
      inputs: [internal_metrics]
      address: 0.0.0.0:9090
    clickhouse:
      type: clickhouse
      inputs:
        - clickhouse_container_logs
      endpoint: "https://clickhouse-internal.pre.gke.gitlab.net:8443"
      auth:
        strategy: basic
        user: "${CLICKHOUSE_USERNAME}"
        password: "${CLICKHOUSE_PASSWORD}"
      database: logs_db
      table: rails_logs_mapped
      skip_unknown_fields: true
      date_time_best_effort: true
      encoding:
        timestamp_format: "unix_ns"

## END VECTOR CONFIG ##
