---
clusterName: {{ .Values.cluster }}

# in addition to these, an empty secret named `secret-snuba-env` needs to be created manually
# the chart breaks if this secret isn't present, but we don't want to use it as
# it will result in the Snuba password being exposed in plaintext
externalSecrets:
  clickhouse-credentials-v1:
    refreshInterval: 0
    secretStoreName: sentry-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/clickhouse-credentials"
          property: snuba-password
          version: "1"
        secretKey: snuba-password
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/clickhouse-credentials"
          property: snuba-password-hashed
          version: "1"
        secretKey: snuba-password-hashed
  postgres-credentials-v1:
    refreshInterval: 0
    secretStoreName: sentry-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/postgres-credentials"
          property: database
          version: "5"
        secretKey: database
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/postgres-credentials"
          property: password
          version: "5"
        secretKey: password
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/postgres-credentials"
          property: username
          version: "5"
        secretKey: username
  sentry-credentials-v1:
    refreshInterval: 0
    secretStoreName: sentry-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/sentry-credentials"
          property: admin-password
          version: "2"
        secretKey: admin-password
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/sentry-credentials"
          property: secret-key
          version: "2"
        secretKey: secret-key
  email-credentials-v1:
    refreshInterval: 0
    secretStoreName: sentry-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/email-credentials"
          property: password
          version: "2"
        secretKey: password
  slack-credentials-v1:
    refreshInterval: 0
    secretStoreName: sentry-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/slack-credentials"
          property: client-id
          version: "2"
        secretKey: client-id
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/slack-credentials"
          property: client-secret
          version: "2"
        secretKey: client-secret
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/slack-credentials"
          property: signing-secret
          version: "2"
        secretKey: signing-secret
  rabbitmq-v1:
    refreshInterval: 0
    secretStoreName: sentry-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/rabbitmq"
          property: password
          version: "1"
        secretKey: rabbitmq-password
      - remoteRef:
          key: "{{ .Values.cluster }}/sentry/rabbitmq"
          property: erlang_cookie
          version: "1"
        secretKey: rabbitmq-erlang-cookie

secretStores:
  - name: sentry-secrets
    role: sentry

serviceAccount:
  name: sentry-secrets
