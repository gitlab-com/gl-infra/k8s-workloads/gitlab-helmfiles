# external-dns

## Usage

This release only works in GKE clusters that have
[workload identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity)
enabled.

This release currently can only create DNS records that are under (subdomains
of) gitlab.net. Extending it to support more domains should be trivial if
needed.

1. Add `external_dns.installed: true` to the relevant environment's values in
   `bases/environments.yaml`.
1. Ensure a managed DNS zone for `$ENV.gke.gitlab.net` exists in the same Google
   project as the GKE cluster you're deploying to, and that it is delegated to
   by an NS record in the gitlab.net root zone.
1. Ensure a GCP service account named `kube-external-dns-$ENV` exists in the
   same Google project.
1. No extra config is needed - deploy!
