---
provider: google

commonLabels:
  deployment: external-dns
  shard: default
  stage: main
  tier: sv
  type: external-dns

serviceAccount:
  annotations:
    iam.gke.io/gcp-service-account: "{{ printf "k8s-dns-%s" .Values.cluster }}@{{ .Values.google_project }}.iam.gserviceaccount.com"

domainFilters:
  - "{{ .Environment.Name }}.gke.gitlab.net"
  - "{{ .Values.cluster }}.{{ .Values.region }}.{{ .Values.google_project }}.gke.gitlab.net"

policy: sync
txtOwnerId: k8s-external-dns-{{ .Environment.Name }}.

publishInternalServices: true

crd:
  create: true

sources:
  - crd
  - service
  - ingress

# Default 1m, also use events to trigger updates
interval: 30s
triggerLoopOnEvent: true

metrics:
  enabled: true
  serviceMonitor:
    enabled: {{ .Values | get "external_dns.serviceMonitor.enabled" "true" }}
    additionalLabels:
      gitlab.com/prometheus-instance: prometheus-system

nodeSelector:
  iam.gke.io/gke-metadata-server-enabled: "true"

resources:
  requests:
    cpu: 200m
    memory: 500Mi
  limits:
    memory: 1Gi
