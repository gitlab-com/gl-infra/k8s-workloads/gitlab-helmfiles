# https://runatlantis.github.io/helm-charts/
---
image:
  repository: us-east1-docker.pkg.dev/gitlab-com-artifact-registry/containers/atlantis
  # renovate: datasource=docker depName=registry.gitlab.com/gitlab-com/gl-infra/ci-images/atlantis versioning=docker depType=ops
  tag: 1.45.0
  pullPolicy: IfNotPresent

orgAllowlist: >-
  ops.gitlab.net/gitlab-com/gl-infra/cells/tissue,ops.gitlab.net/gitlab-com/gl-infra/config-mgmt

basicAuthSecretName: atlantis-auth-v1

containerSecurityContext:
  allowPrivilegeEscalation: false
  readOnlyRootFilesystem: true

environment:
  ATLANTIS_ALLOW_COMMANDS: version,plan,apply,unlock,approve_policies
  ATLANTIS_ALLOW_DRAFT_PRS: 'true'
  ATLANTIS_AUTODISCOVER_MODE: 'disabled'
  ATLANTIS_AUTOPLAN_MODULES: 'true'
  ATLANTIS_CHECKOUT_DEPTH: '200'
  ATLANTIS_CHECKOUT_STRATEGY: merge
  ATLANTIS_DISABLE_AUTOPLAN_LABEL: no-autoplan
  ATLANTIS_EMOJI_REACTION: trident
  ATLANTIS_ENABLE_DIFF_MARKDOWN_FORMAT: 'true'
  ATLANTIS_ENABLE_POLICY_CHECKS: 'true'
  ATLANTIS_HIDE_PREV_PLAN_COMMENTS: 'true'
  ATLANTIS_QUIET_POLICY_CHECKS: 'true'
  TF_CLI_CONFIG_FILE: /terraformrc/atlantis.tfrc

resources:
  requests:
    cpu: 1000m
    memory: 2Gi
  limits:
    cpu: 2000m
    memory: 4Gi

volumeClaim:
  enabled: true
  dataStorage: 10Gi
  storageClassName: pd-balanced

extraVolumes:
  - name: terraformrc
    secret:
      secretName: terraformrc

extraVolumeMounts:
  - name: terraformrc
    mountPath: /terraformrc

podTemplate:
  annotations:
    cluster-autoscaler.kubernetes.io/safe-to-evict: "false"
  labels: &podLabels
    type: atlantis
    deployment: atlantis
    tier: inf
    shard: default
    stage: main

statefulSet:
  updateStrategy:
    type: RollingUpdate
  labels: *podLabels

lockingDbType: redis

redis:
  host: redis-master
  port: 6379
  db: 0

redisSecretName: redis-v1

servicemonitor:
  enabled: true
  interval: 15s
  path: /metrics
  auth:
    externalSecret:
      enabled: true
      name: atlantis-auth-v1
      keys:
        username: username
        password: password
