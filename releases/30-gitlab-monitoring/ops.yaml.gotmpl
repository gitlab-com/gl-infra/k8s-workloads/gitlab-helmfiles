---
alertmanager:
  alertmanagerSpec:
    replicas: {{ .Values | get "gitlab_monitoring.alertmanager.replicas" nil }}
    podAntiAffinity: hard
    topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: topology.kubernetes.io/zone
        whenUnsatisfiable: ScheduleAnyway
        labelSelector:
          matchLabels:
            app.kubernetes.io/name: alertmanager
            alertmanager: gitlab-monitoring-promethe-alertmanager
    storage:
      volumeClaimTemplate:
        spec:
          storageClassName: ssd
          resources:
            requests:
              storage: 10Gi
    containers:
      - name: alertmanager
        livenessProbe:
          # Give enough time for IP to propagate by DNS
          initialDelaySeconds: 30
    # TODO: remove once discovery A records switch to SRV targeting the SVCs https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/14170
    minReadySeconds: 60
  podDisruptionBudget:
    enabled: true
    minAvailable: 2
  ingress:
    hosts:
      - {{ .Values | get "gitlab_monitoring.alertmanager.domain" nil }}
    paths:
      - /*
  servicePerReplica:
    enabled: true
    annotations:
      cloud.google.com/neg: '{"ingress": true}'
      cloud.google.com/backend-config: '{"ports": {"web":"alertmanager-replica"}}'
      networking.gke.io/load-balancer-type: Internal
      networking.gke.io/internal-load-balancer-allow-global-access: "true"
      # TODO: add these annotations once operator + chart are upgraded https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/14419
      # external-dns.alpha.kubernetes.io/hostname: "alertmanager-internal-$i.ops.gke.gitlab.net."
      # external-dns.alpha.kubernetes.io/ttl: "10"
    type: LoadBalancer
  ingressPerReplica:
    enabled: true
    # <hostPrefix>-<replicaNumber>.<hostDomain>
    hostPrefix: alertmanager
    hostDomain: "{{ .Environment.Name }}.gke.gitlab.net"
    pathType: Prefix
    paths:
      - /
    annotations:
      external-dns.alpha.kubernetes.io/ingress-hostname-source: "defined-hosts-only"
      # Expire frequently to discover changed/new alertmanager nodes every
      # time the prometheus check loop runs - 30s.
      external-dns.alpha.kubernetes.io/ttl: "10"
      # GCP LB
      networking.gke.io/managed-certificates: "alertmanager-replica"
      networking.gke.io/v1beta1.FrontendConfig: "alertmanager"

prometheus:
  enabled: false
  prometheusSpec:
    # release notes https://github.com/prometheus/prometheus/releases
    # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker depType=ops
    version: v2.55.1
    # See: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24728
    image:
      # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker depType=ops
      tag: v2.55.1
    enableFeatures:
      # https://github.com/prometheus/prometheus/pull/10498
      - auto-gomaxprocs
      # TODO(rnaveiras): Move to the default values after is enabled in all Prometheus
      # Requires Prometheus >= 2.30
      # Prometheus writes out a more raw snapshot of its current in-memory
      # state upon shutdown, which can then be re-read into memory more
      # efficiently when the server restarts.
      # Reduce starts time in 50-80%, increasing fast recoverability
      # https://github.com/prometheus/prometheus/pull/7229
      - memory-snapshot-on-shutdown
    # The ops environment already talks to the ops alertmanagers via kubernetes_sd_configs.
    additionalAlertManagerConfigs: []
    replicas: 2
    walCompression: true
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: ssd
          resources:
            requests:
              storage: 200Gi
    resources:
      requests:
        cpu: "4"
        memory: 25Gi
      limits:
        memory: 25Gi
    # required for kube-prometheus-stack v52
    thanos:
      objectStorageConfig:
        existingSecret:
          key: objstore.yaml
          name: prometheus-thanos-objstore-v3
    serviceMonitorNamespaceSelector:
      matchExpressions:
        - key: kubernetes.io/metadata.name
          operator: NotIn
          values:
            - mimir
    podMonitorNamespaceSelector:
      matchExpressions:
        - key: kubernetes.io/metadata.name
          operator: NotIn
          values:
            - mimir
    ruleNamespaceSelector:
      matchExpressions:
        - key: kubernetes.io/metadata.name
          operator: NotIn
          values:
            - mimir
    probeNamespaceSelector:
      matchExpressions:
        - key: kubernetes.io/metadata.name
          operator: NotIn
          values:
            - mimir
    scrapeConfigNamespaceSelector:
      matchExpressions:
        - key: kubernetes.io/metadata.name
          operator: NotIn
          values:
            - mimir
  ingress:
    hosts:
      - prometheus-gke.ops.gitlab.net
    paths:
      - /*
  service:
    loadBalancerIP: "10.250.26.2"

