---
fullnameOverride: "runway-rw"
prometheus:
  enabled: true
  agentMode: true
  thanosService:
    enabled: false
  podDisruptionBudget:
    enabled: true
  service:
    clusterIP: ""
    type: ClusterIP
    additionalPorts: []
  prometheusSpec:
    enableFeatures:
      # https://github.com/prometheus/prometheus/pull/10498
      - auto-gomaxprocs
    additionalArgs: ""
    replicas: 2
    replicaExternalLabelName: __replica__
    remoteWrite:
      - url: https://mimir.ops.gke.gitlab.net/api/v1/push
        name: mimir
        headers:
          X-Scope-OrgID: runway
        basicAuth:
          username:
            name: prometheus-remote-write-auth-runway
            key: username
          password:
            name: prometheus-remote-write-auth-runway
            key: password
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: pd-ssd
          resources:
            requests:
              storage: 10Gi
    resources:
      requests:
        cpu: "2"
        memory: 8Gi
      limits:
        memory: 8Gi

    serviceMonitorSelectorNilUsesHelmValues: false
    serviceMonitorSelector:
      # Label set in https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/runway-exporter
      matchLabels:
        gitlab.com/prometheus-collector: runway

    podMonitorSelectorNilUsesHelmValues: false
    podMonitorSelector:
      matchLabels:
        gitlab.com/prometheus-collector: runway

    ruleSelectorNilUsesHelmValues: true
    ruleNamespaceSelector: ""
    ruleSelector:
      matchLabels:
        gitlab.com/prometheus-collector: runway

    scrapeConfigSelector:
      matchLabels:
        gitlab.com/prometheus-collector: runway

    probeSelector:
      matchLabels:
        gitlab.com/prometheus-collector: runway

    secrets: null
    additionalScrapeConfigs: null

  serviceAccount:
    annotations:
      iam.gke.io/gcp-service-account: prometheus-rw-{{ .Values | get "env_prefix" .Environment.Name }}@{{ .Values.google_project }}.iam.gserviceaccount.com
