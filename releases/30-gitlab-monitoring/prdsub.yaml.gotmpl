---
prometheus:
  enabled: false
  prometheusSpec:
    # release notes https://github.com/prometheus/prometheus/releases
    # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker depType=prdsub
    version: v2.55.1
    # See: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24728
    image:
      # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker depType=prdsub
      tag: v2.55.1
    additionalScrapeConfigs:
      - job_name: customerdot
        relabel_configs:
          - target_label: instance
            source_labels:
              - __meta_gce_instance_name
          - target_label: dc
            source_labels:
              - __meta_gce_zone
            regex: ".*/([^/]+)$"
            replacement: gce-$1
          - target_label: environment
            replacement: gprd
          - target_label: tier
            replacement: sv
          - target_label: type
            replacement: customersdot
          - target_label: stage
            replacement: main
          - target_label: shard
            replacement: default
          - target_label: service
            replacement: customersdot
          - target_label: node
            source_labels:
              - __meta_gce_instance_name
        gce_sd_configs:
        {{- range list "us-east1-b" "us-east1-c" "us-east1-d" }}
        - project: gitlab-subscriptions-prod
          zone: {{ . }}
          filter: name = "customers-*"
        {{- end }}
      - job_name: 'prometheus-puma-exporter'
        relabel_configs:
          - target_label: instance
            source_labels:
              - __meta_gce_instance_name
          - target_label: dc
            source_labels:
              - __meta_gce_zone
            regex: ".*/([^/]+)$"
            replacement: gce-$1
          - target_label: environment
            replacement: gprd
          - target_label: tier
            replacement: sv
          - target_label: type
            replacement: customersdot
          - target_label: stage
            replacement: main
          - target_label: shard
            replacement: default
          - target_label: service
            replacement: customersdot
          - target_label: node
            source_labels:
              - __meta_gce_instance_name
        gce_sd_configs:
        {{- range list "us-east1-b" "us-east1-c" "us-east1-d" }}
        - project: gitlab-subscriptions-prod
          zone: {{ . }}
          filter: name = "customers-*"
          port: 9090
        {{- end }}
      - job_name: node
        relabel_configs:
          - target_label: instance
            source_labels:
              - __meta_gce_instance_name
          - target_label: dc
            source_labels:
              - __meta_gce_zone
            regex: ".*/([^/]+)$"
            replacement: gce-$1
          - target_label: environment
            replacement: gprd
          - target_label: tier
            replacement: monitoring
          - target_label: type
            replacement: customersdot
          - target_label: stage
            replacement: main
          - target_label: shard
            replacement: default
        gce_sd_configs:
        {{- range list "us-east1-b" "us-east1-c" "us-east1-d" }}
        - project: gitlab-subscriptions-prod
          zone: {{ . }}
          filter: name = "customers-*"
          port: 9100
        {{- end }}
      - job_name: 'process_exporter'
        relabel_configs:
          - target_label: instance
            source_labels:
              - __meta_gce_instance_name
          - target_label: dc
            source_labels:
              - __meta_gce_zone
            regex: ".*/([^/]+)$"
            replacement: gce-$1
          - target_label: environment
            replacement: gprd
          - target_label: tier
            replacement: sv
          - target_label: type
            replacement: customersdot
          - target_label: stage
            replacement: main
          - target_label: shard
            replacement: default
          - target_label: service
            replacement: customersdot
          - target_label: node
            source_labels:
              - __meta_gce_instance_name
        gce_sd_configs:
        {{- range list "us-east1-b" "us-east1-c" "us-east1-d" }}
        - project: gitlab-subscriptions-prod
          zone: {{ . }}
          filter: name = "customers-*"
          port: 9256
        {{- end }}
    replicas: 2
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: standard-regional-east1
          resources:
            requests:
              storage: 200Gi
    resources:
      requests:
        cpu: "1"
        memory: 5Gi
  ingress:
    hosts:
      - prometheus-gke.prdsub.gitlab.net
    paths:
      - /*
  service:
    loadBalancerIP: "10.185.8.2"

