---
prometheus:
  enabled: true
  prometheusSpec:
    # release notes https://github.com/prometheus/prometheus/releases
    # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker depType=gstg
    version: v2.55.1
    # See: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/24728
    image:
      # renovate: datasource=docker depName=quay.io/prometheus/prometheus versioning=docker depType=gstg
      tag: v2.55.1
    resources:
      requests:
        cpu: 1500m
        memory: 10Gi
      limits:
        cpu: 3000m
        memory: 20Gi
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: pd-balanced
          resources:
            requests:
              storage: 50Gi
    secrets: null
    additionalScrapeConfigs: null

  service:
    loadBalancerIP: ""

  ingress:
    enabled: false

kubelet:
  serviceMonitor:
    # https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/values.yaml#L970
    ## MetricRelabelConfigs to apply to samples after scraping, but before ingestion.
    ## ref: https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/api.md#relabelconfig
    ##
    cAdvisorMetricRelabelings:
      # Drop less useful container CPU metrics.
      - sourceLabels: [__name__]
        action: drop
        # TODO: check if we can drop `container_cpu_cfs_throttled_seconds_total`
        # regex: 'container_cpu_(cfs_throttled_seconds_total|load_average_10s|system_seconds_total|user_seconds_total)'
        regex: 'container_cpu_(load_average_10s|system_seconds_total|user_seconds_total)'
      # Drop less useful container / always zero filesystem metrics.
      - sourceLabels: [__name__]
        action: drop
        regex: 'container_fs_(io_current|io_time_seconds_total|io_time_weighted_seconds_total|reads_merged_total|sector_reads_total|sector_writes_total|writes_merged_total)'
      # Drop less useful / always zero container memory metrics.
      - sourceLabels: [__name__]
        action: drop
        regex: 'container_memory_(mapped_file|swap)'
      # Drop less useful container process metrics.
      - sourceLabels: [__name__]
        action: drop
        regex: 'container_(file_descriptors|tasks_state|threads_max)'
      # Add any id matching a git cgroup to a temporary label
      - sourceLabels: [id]
        regex: (.*/repos-.*)
        action: replace
        targetLabel: tmp_git_cgroup_id
      # Use temporary label to evaluate against, and continue dropping empty pods only if git_cgroup_id is also not set or empty.
      - sourceLabels: [id, pod, tmp_git_cgroup_id]
        regex: .+;;
        action: drop
      # Cleanup temp label.
      - action: labeldrop
        regex: tmp_git_cgroup_id
      # Set fqdn on gitaly pods for backwards compatibility with VMs.
      - sourceLabels:
          - pod
        action: replace
        regex: (gitaly-k8s.*)
        targetLabel: fqdn
      - sourceLabels:
          - pod
        action: replace
        replacement: gitaly
        regex: (gitaly-k8s.*)
        targetLabel: type
