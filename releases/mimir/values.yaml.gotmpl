global:
  podLabels:
    type: mimir
    deployment: mimir
    stage: main
    tier: inf

serviceAccount:
  create: true
  name: mimir
  annotations:
    iam.gke.io/gcp-service-account: mimir-{{ .Environment.Name }}@{{ .Values.google_project }}.iam.gserviceaccount.com

# This is for special use cases where we want a user or set of credentials
# to be able to set the X-Scope-OrgID to any value as needed
multi_tenant_users:
  - gitlab
  - mimir
  - runbooks
  - release-tools
  - gitaly-shard-weights-assigner
  - keda
  - patching-notifier

runtimeConfigMount: &runtimeConfigMount
  extraVolumes:
    - name: runtime-config-generated
      secret:
        secretName: mimir-runtimeconfig-generated
  extraVolumeMounts:
    - name: runtime-config-generated
      readOnly: true
      mountPath: /config

kedaAutoscaling:
  prometheusAddress: http://{{ .Release.Name }}-query-frontend.{{ .Release.Namespace }}.svc:8080/prometheus
  customHeaders:
    X-Scope-OrgID: metamonitoring

# Runtime config https://grafana.com/docs/mimir/latest/configure/about-runtime-configuration/
runtimeConfig:
  # IMPORTANT: Runtime config/overrides, are now set with the tenant file in config-mgmt.
  # See: https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/tree/master/environments/observability-tenants.

# https://github.com/grafana/mimir/blob/main/operations/helm/charts/mimir-distributed/values.yaml
mimir:
  structuredConfig:
    # runtime config generated via https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/tree/master/environments/observability-tenants
    runtime_config:
      file: /config/runtime.yaml

    usage_stats:
      enabled: false

    # Default limits and config for tenants
    limits:
      accept_ha_samples: true
      ha_cluster_label: cluster
      ha_replica_label: __replica__
      cardinality_analysis_enabled: true
      # Shard defaults
      ingestion_tenant_shard_size: 4
      max_queriers_per_tenant: 4
      store_gateway_tenant_shard_size: 4
      ruler_tenant_shard_size: 4
      ruler_max_rules_per_rule_group: 100
      ruler_max_rule_groups_per_tenant: 100
      compactor_tenant_shard_size: 1
      # Default retention
      compactor_blocks_retention_period: 1y
      # Period to accept out of order samples
      out_of_order_time_window: 30m

    server:
      log_format: json

    frontend:
      # (experimental) If a querier disconnects without sending notification about
      # graceful shutdown, the query-frontend will keep the querier in the tenant's 
      # shard until the forget delay has passed. This feature is useful to reduce the
      # blast radius when shuffle-sharding is enabled.
      querier_forget_delay: 30s
      query_stats_enabled: false
      log_queries_longer_than: 30s

    blocks_storage:
      backend: gcs
      gcs:
        bucket_name: mimir-metrics-multi-{{ .Environment.Name }}
    ruler_storage:
      backend: gcs
      gcs:
        bucket_name: mimir-ruler-multi-{{ .Environment.Name }}
    alertmanager_storage:
      backend: gcs
      gcs:
        bucket_name: mimir-alertmanager-multi-{{ .Environment.Name }}
    tenant_federation:
      enabled: true
    ruler:
      external_url: "https://alerts.gitlab.net"
      alertmanager_url: dnssrvnoa+http://_web._tcp.alertmanager-headless.monitoring.svc.cluster.local
      query_frontend:
        address: dns:///mimir-query-frontend.mimir.svc.cluster.local:9095
      tenant_federation:
        enabled: true
    distributor:
      remote_timeout: 5s
      ha_tracker:
        enable_ha_tracker: true
        kvstore:
          store: consul
          consul:
            host: http://mimir-consul-consul-server.mimir.svc.cluster.local:8500
      ring:
        heartbeat_period: 1m
        heartbeat_timeout: 4m
    ingester:
      ring:
        heartbeat_period: 2m
        heartbeat_timeout: 10m
    ingester_client:
      grpc_client_config:
        grpc_compression: snappy
    store_gateway:
      sharding_ring:
        heartbeat_period: 1m
        heartbeat_timeout: 4m

ingester:
  <<: *runtimeConfigMount
  rollout_operator:
    enabled: true
  persistentVolume:
    size: 10Gi
    storageClass: pd-balanced
  topologySpreadConstraints: ""
  zoneAwareReplication:
    topologyKey: 'kubernetes.io/hostname'
    zones:
      - name: us-east1-b
        nodeSelector:
          topology.kubernetes.io/zone: us-east1-b
      - name: us-east1-c
        nodeSelector:
          topology.kubernetes.io/zone: us-east1-c
      - name: us-east1-d
        nodeSelector:
          topology.kubernetes.io/zone: us-east1-d

store_gateway:
  <<: *runtimeConfigMount
  persistentVolume:
    size: 10Gi
    storageClass: pd-ssd
  topologySpreadConstraints: ""
  affinity:
    podAntiAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchExpressions:
              - key: target # support for enterprise.legacyLabels
                operator: In
                values:
                  - store-gateway
          topologyKey: 'kubernetes.io/hostname'

        - labelSelector:
            matchExpressions:
              - key: app.kubernetes.io/component
                operator: In
                values:
                  - store-gateway
          topologyKey: 'kubernetes.io/hostname'
  rollout_operator:
    enabled: true
  zoneAwareReplication:
    topologyKey: 'kubernetes.io/hostname'
    zones:
      - name: us-east1-b
        nodeSelector:
          topology.kubernetes.io/zone: us-east1-b
      - name: us-east1-c
        nodeSelector:
          topology.kubernetes.io/zone: us-east1-c
      - name: us-east1-d
        nodeSelector:
          topology.kubernetes.io/zone: us-east1-d

compactor:
  <<: *runtimeConfigMount
  replicas: 3

ruler:
  <<: *runtimeConfigMount
  enabled: true

alertmanager:
  <<: *runtimeConfigMount

distributor:
  <<: *runtimeConfigMount
  podDisruptionBudget:
    maxUnavailable: 10%
  kedaAutoscaling:
    enabled: true
    minReplicaCount: 1
    maxReplicaCount: 6
    targetCPUUtilizationPercentage: 80
    targetMemoryUtilizationPercentage: 80
    behavior:
      scaleDown:
        policies:
          - periodSeconds: 600
            type: Percent
            value: 10

querier:
  <<: *runtimeConfigMount
  podDisruptionBudget:
    maxUnavailable: 10%
  kedaAutoscaling:
    enabled: true
    minReplicaCount: 1
    maxReplicaCount: 6
    querySchedulerInflightRequestsThreshold: 12
    behavior:
      scaleDown:
        policies:
          - periodSeconds: 120
            type: Percent
            value: 10
        stabilizationWindowSeconds: 600
      scaleUp:
        policies:
          - periodSeconds: 120
            type: Percent
            value: 50
          - periodSeconds: 120
            type: Pods
            value: 15
        stabilizationWindowSeconds: 60

query_frontend:
  <<: *runtimeConfigMount
  kedaAutoscaling:
    enabled: true
    minReplicaCount: 1
    maxReplicaCount: 6
    targetCPUUtilizationPercentage: 80
    targetMemoryUtilizationPercentage: 80
    behavior:
      scaleDown:
        policies:
          - periodSeconds: 60
            type: Percent
            value: 10

query_scheduler:
  <<: *runtimeConfigMount

minio:
  enabled: false

gateway:
  <<: *runtimeConfigMount
  enabledNonEnterprise: true
  resources:
    limits:
      memory: 731Mi
    requests:
      cpu: 2
      memory: 512Mi
  autoscaling:
    enabled: true
    minReplicas: 3
    maxReplicas: 12
    targetCPUUtilizationPercentage: 70
    targetMemoryUtilizationPercentage: 70
  service:
    annotations:
      cloud.google.com/neg: '{"ingress": true}'
      cloud.google.com/backend-config: '{"default": "mimir-gateway"}'
  ingress:
    enabled: true
    annotations:
      cert-manager.io/cluster-issuer: gitlab-combined
      external-dns.alpha.kubernetes.io/hostname: mimir.{{ .Environment.Name }}.gke.gitlab.net,mimir.{{ .Values.cluster }}.{{ .Values.region }}.{{ .Values.google_project }}.gke.gitlab.net
      kubernetes.io/ingress.allow-http: "false"
    hosts:
      - host: mimir.{{ .Environment.Name }}.gke.gitlab.net
        paths:
          - path: '/api/v1/push'
            pathType: ImplementationSpecific
          - path: '/otlp/v1/metrics'
            pathType: ImplementationSpecific
      - host: mimir.{{ .Values.cluster }}.{{ .Values.region }}.{{ .Values.google_project }}.gke.gitlab.net
        paths:
          - path: '/api/v1/push'
            pathType: ImplementationSpecific
          - path: '/otlp/v1/metrics'
            pathType: ImplementationSpecific
    tls:
      - secretName: mimir-tls
        hosts:
          - mimir.{{ .Environment.Name }}.gke.gitlab.net
          - mimir.{{ .Values.cluster }}.{{ .Values.region }}.{{ .Values.google_project }}.gke.gitlab.net
  extraContainers:
    - name: nginx-exporter
      # renovate: datasource=docker depName=bitnami/nginx-exporter versioning=docker
      image: bitnami/nginx-exporter:1.1.0-debian-12-r7
      imagePullPolicy: IfNotPresent
      command:
        - exporter
      args:
        - --nginx.scrape-uri
        - "http://127.0.0.1:8080/status"
        - --web.listen-address
        - ":9113"
      ports:
        - name: metrics
          containerPort: 9113
      livenessProbe:
        httpGet:
          path: /metrics
          port: metrics
        initialDelaySeconds: 15
        timeoutSeconds: 5
      readinessProbe:
        httpGet:
          path: /metrics
          port: metrics
        initialDelaySeconds: 5
        timeoutSeconds: 1
      resources:
        requests:
          cpu: 20m
          memory: 128Mi
        limits:
          cpu: 100m
          memory: 248Mi
  nginx:
    enabled: false
    verboseLogging: false
    basicAuth:
      enabled: true
      existingSecret: mimir-auth-generated

    config:
      {{`
      file: |
        worker_processes  5;  ## Default: 1
        error_log  /dev/stderr {{ .Values.gateway.nginx.config.errorLogLevel }};
        pid        /tmp/nginx.pid;
        worker_rlimit_nofile 8192;

        events {
          worker_connections  4096;  ## Default: 1024
        }

        http {
          client_body_temp_path /tmp/client_temp;
          proxy_temp_path       /tmp/proxy_temp_path;
          fastcgi_temp_path     /tmp/fastcgi_temp;
          uwsgi_temp_path       /tmp/uwsgi_temp;
          scgi_temp_path        /tmp/scgi_temp;

          default_type application/octet-stream;
          log_format   {{ .Values.gateway.nginx.config.logFormat }}

          {{- if .Values.gateway.nginx.verboseLogging }}
          access_log   /dev/stderr  main;
          {{- else }}

          map $status $loggable {
            ~^[23]  0;
            default 1;
          }
          access_log   {{ .Values.gateway.nginx.config.accessLogEnabled | ternary "/dev/stderr  main  if=$loggable;" "off;" }}
          {{- end }}

          sendfile     on;
          tcp_nopush   on;

          {{- if .Values.gateway.nginx.config.resolver }}
          resolver {{ .Values.gateway.nginx.config.resolver }};
          {{- else }}
          resolver {{ .Values.global.dnsService }}.{{ .Values.global.dnsNamespace }}.svc.{{ .Values.global.clusterDomain }};
          {{- end }}

          {{- with .Values.gateway.nginx.config.httpSnippet }}
          {{ . | nindent 2 }}
          {{- end }}

          # Ensure that X-Scope-OrgID is always present, default to the no_auth_tenant for backwards compatibility when multi-tenancy was turned off.
          map $http_x_scope_orgid $ensured_x_scope_orgid {
           default $http_x_scope_orgid;
           "" "{{ include "mimir.noAuthTenant" . }}";
          }
          
          proxy_read_timeout 300;
          server {
            listen {{ include "mimir.serverHttpListenPort" . }};
            listen [::]:{{ include "mimir.serverHttpListenPort" . }};

            {{- if .Values.gateway.nginx.basicAuth.enabled }}
            auth_basic           "Mimir";
            auth_basic_user_file /etc/nginx/secrets/.htpasswd;
            {{- end }}

            # Readiness/Liveness Probes 
            location = / {
              return 200 'OK';
              auth_basic off;
            }

            location = /status {
              stub_status on;
              auth_basic off;
              allow 127.0.0.1;
              deny all;
            }

            location = /ready {
              return 200 'OK';
              auth_basic off;
            }

            # Set Tenant Headers

            # If a user is not allowed to specifi their own tenant header, 
            # we set tenant header to their username.
            if ($remote_user !~ "^({{ join "|" .Values.multi_tenant_users}})$") {
              set $ensured_x_scope_orgid $remote_user;
            }

            proxy_set_header X-Scope-OrgID $ensured_x_scope_orgid;

            # Distributor endpoints
            location /distributor {
              set $distributor {{ template "mimir.fullname" . }}-distributor-headless.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$distributor:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }
            location = /api/v1/push {
              set $distributor {{ template "mimir.fullname" . }}-distributor-headless.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$distributor:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }
            location /otlp/v1/metrics {
              set $distributor {{ template "mimir.fullname" . }}-distributor-headless.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$distributor:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }

            # Alertmanager endpoints
            location {{ template "mimir.alertmanagerHttpPrefix" . }} {
              set $alertmanager {{ template "mimir.fullname" . }}-alertmanager-headless.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$alertmanager:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }
            location = /multitenant_alertmanager/status {
              set $alertmanager {{ template "mimir.fullname" . }}-alertmanager-headless.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$alertmanager:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }
            location = /api/v1/alerts {
              set $alertmanager {{ template "mimir.fullname" . }}-alertmanager-headless.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$alertmanager:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }

            # Ruler endpoints
            location {{ template "mimir.prometheusHttpPrefix" . }}/config/v1/rules {
              set $ruler {{ template "mimir.fullname" . }}-ruler.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$ruler:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }
            location {{ template "mimir.prometheusHttpPrefix" . }}/api/v1/rules {
              set $ruler {{ template "mimir.fullname" . }}-ruler.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$ruler:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }

            location {{ template "mimir.prometheusHttpPrefix" . }}/api/v1/alerts {
              set $ruler {{ template "mimir.fullname" . }}-ruler.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$ruler:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }
            location = /ruler/ring {
              set $ruler {{ template "mimir.fullname" . }}-ruler.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$ruler:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }

            # Rest of {{ template "mimir.prometheusHttpPrefix" . }} goes to the query frontend
            location {{ template "mimir.prometheusHttpPrefix" . }} {
              set $query_frontend {{ template "mimir.fullname" . }}-query-frontend.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$query_frontend:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }

            # Buildinfo endpoint can go to any component
            location = /api/v1/status/buildinfo {
              set $query_frontend {{ template "mimir.fullname" . }}-query-frontend.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$query_frontend:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }

            # Compactor endpoint for uploading blocks
            location /api/v1/upload/block/ {
              set $compactor {{ template "mimir.fullname" . }}-compactor.{{ .Release.Namespace }}.svc.{{ .Values.global.clusterDomain }};
              proxy_pass      http://$compactor:{{ include "mimir.serverHttpListenPort" . }}$request_uri;
            }

            # Fake endpoints for thanos
            location /prometheus/api/v1/status/config {
              add_header Content-Type application/json;
              return 200 "{\"status\":\"success\",\"data\":{\"yaml\": \"global:\\n  external_labels:\\n    source: mimir\"}}";
              auth_basic off;
            }

            location /prometheus/api/v1/status/buildinfo {
              add_header Content-Type application/json;
              return 200 "{\"status\":\"success\",\"data\":{\"version\":\"2.45.0\",\"revision\":\"8ef767e396bf8445f009f945b0162fd71827f445\",\"branch\":\"HEAD\",\"buildUser\":\"root@920118f645b7\",\"buildDate\":\"20230623-15:09:49\",\"goVersion\":\"go1.20.5\"}}";
              auth_basic off;
            }

            {{- with .Values.gateway.nginx.config.serverSnippet }}
            {{ . | nindent 4 }}
            {{- end }}
          }
        }
      `}}

## Memcached
memcached:
  image:
    # renovate: datasource=docker depName=memcached versioning=docker
    tag: 1.6.37-alpine

memcachedExporter:
  image:
    # renovate: datasource=docker depName=prom/memcached-exporter versioning=docker
    tag: v0.15.1

chunks-cache:
  enabled: true

index-cache:
  enabled: true

metadata-cache:
  enabled: true

results-cache:
  enabled: true

## Exporter
overrides_exporter:
  <<: *runtimeConfigMount
  replicas: 1

## MetaMonitoring
metaMonitoring:
  serviceMonitor:
    enabled: true
    relabelings:
      - action: replace
        sourceLabels: [__metrics_path__]
        regex: (/metrics/cadvisor)
        targetLabel: metrics_path
      - action: replace
        replacement: mimir
        targetLabel: type
      - action: replace
        replacement: {{ .Environment.Name }}
        targetLabel: env
      - action: replace
        replacement: {{ .Environment.Name }}
        targetLabel: environment
  grafanaAgent:
    enabled: true
    installOperator: true
    logs:
      enabled: false
    metrics:
      enabled: true
      remote:
        auth:
          username: mimir
          passwordSecretName: mimir-metamon-auth
          passwordSecretKey: password
      scrapeInterval: 15s
      scrapeK8s:
        enabled: true
        namespace: monitoring

## Bucket Creation and IAM Policies
extraObjects:
  - apiVersion: core.cnrm.cloud.google.com/v1beta1
    kind: ConfigConnectorContext
    metadata:
      name: configconnectorcontext.core.cnrm.cloud.google.com
    spec:
      googleServiceAccount: mimir-cc@{{ .Values.google_project }}.iam.gserviceaccount.com

  # Multi-Region Buckets
  - apiVersion: storage.cnrm.cloud.google.com/v1beta1
    kind: StorageBucket
    metadata:
      name: mimir-metrics-multi-{{ .Environment.Name }}
      annotations:
        cnrm.cloud.google.com/project-id: gitlab-observability
        cnrm.cloud.google.com/deletion-policy: abandon
        cnrm.cloud.google.com/force-destroy: "false"
    spec:
      location: US
      storageClass: MULTI_REGIONAL
      uniformBucketLevelAccess: true
      # Disable versioning in favour of soft delete
      # https://cloud.google.com/resources/storage/soft-delete-announce
      versioning:
        enabled: false

  - apiVersion: iam.cnrm.cloud.google.com/v1beta1
    kind: IAMPolicyMember
    metadata:
      name: mimir-metrics-multi-{{ .Environment.Name }}
      annotations:
        cnrm.cloud.google.com/project-id: gitlab-observability
    spec:
      member: serviceAccount:mimir-{{ .Environment.Name }}@{{ .Values.google_project }}.iam.gserviceaccount.com
      resourceRef:
        apiVersion: storage.cnrm.cloud.google.com/v1beta1
        kind: StorageBucket
        name: mimir-metrics-multi-{{ .Environment.Name }}
      role: roles/storage.objectAdmin

  - apiVersion: storage.cnrm.cloud.google.com/v1beta1
    kind: StorageBucket
    metadata:
      name: mimir-ruler-multi-{{ .Environment.Name }}
      annotations:
        cnrm.cloud.google.com/project-id: gitlab-observability
        cnrm.cloud.google.com/deletion-policy: abandon
        cnrm.cloud.google.com/force-destroy: "false"
    spec:
      location: US
      storageClass: MULTI_REGIONAL
      uniformBucketLevelAccess: true
      # Disable versioning in favour of soft delete
      # https://cloud.google.com/resources/storage/soft-delete-announce
      versioning:
        enabled: false

  - apiVersion: iam.cnrm.cloud.google.com/v1beta1
    kind: IAMPolicyMember
    metadata:
      name: mimir-ruler-multi-{{ .Environment.Name }}
      annotations:
        cnrm.cloud.google.com/project-id: gitlab-observability
    spec:
      member: serviceAccount:mimir-{{ .Environment.Name }}@{{ .Values.google_project }}.iam.gserviceaccount.com
      resourceRef:
        apiVersion: storage.cnrm.cloud.google.com/v1beta1
        kind: StorageBucket
        name: mimir-ruler-multi-{{ .Environment.Name }}
      role: roles/storage.objectAdmin

  - apiVersion: storage.cnrm.cloud.google.com/v1beta1
    kind: StorageBucket
    metadata:
      name: mimir-alertmanager-multi-{{ .Environment.Name }}
      annotations:
        cnrm.cloud.google.com/project-id: gitlab-observability
        cnrm.cloud.google.com/deletion-policy: abandon
        cnrm.cloud.google.com/force-destroy: "false"
    spec:
      location: US
      storageClass: MULTI_REGIONAL
      uniformBucketLevelAccess: true
      # Disable versioning in favour of soft delete
      # https://cloud.google.com/resources/storage/soft-delete-announce
      versioning:
        enabled: false
          
  - apiVersion: iam.cnrm.cloud.google.com/v1beta1
    kind: IAMPolicyMember
    metadata:
      name: mimir-alertmanager-multi-{{ .Environment.Name }}
      annotations:
        cnrm.cloud.google.com/project-id: gitlab-observability
    spec:
      member: serviceAccount:mimir-{{ .Environment.Name }}@{{ .Values.google_project }}.iam.gserviceaccount.com
      resourceRef:
        apiVersion: storage.cnrm.cloud.google.com/v1beta1
        kind: StorageBucket
        name: mimir-alertmanager-multi-{{ .Environment.Name }}
      role: roles/storage.objectAdmin

  # Additional internal IP Istio VirtualService
  - apiVersion: networking.istio.io/v1beta1
    kind: VirtualService
    metadata:
      labels:
        app.kubernetes.io/component: gateway
        app.kubernetes.io/instance: mimir
        app.kubernetes.io/name: mimir
      name: mimir-gateway-internal
    spec:
      gateways:
      - istio-ingress/internal
      hosts:
      - mimir-internal.{{ .Environment.Name }}.gke.gitlab.net
      - mimir-internal.{{ .Values.cluster }}.{{ .Values.region }}.{{ .Values.google_project }}.gke.gitlab.net
      http:
      - route:
        - destination:
            host: mimir-gateway
            port:
              number: 80

  # BackendConfig for mimir-gateway
  - apiVersion: cloud.google.com/v1
    kind: BackendConfig
    metadata:
      name: mimir-gateway
    spec:
      logging:
        enable: true
        sampleRate: 0.1

  # Deploy custom serviceMonitor here until the port is configurable upstream
  - apiVersion: monitoring.coreos.com/v1
    kind: ServiceMonitor
    metadata:
      labels:
        app.kubernetes.io/component: meta-monitoring
        app.kubernetes.io/instance: mimir
        app.kubernetes.io/name: mimir
      name: mimir-kube-state-metrics
      namespace: mimir
    spec:
      endpoints:
      - honorLabels: true
        metricRelabelings:
        - action: keep
          regex: (^|.*;)((keda-hpa-)?mimir.*)
          separator: ;
          sourceLabels:
          - deployment
          - statefulset
          - pod
          - horizontalpodautoscaler
        path: /metrics
        port: http
        relabelings:
        - action: replace
          replacement: mimir
          targetLabel: cluster
        - action: replace
          replacement: mimir
          targetLabel: type
        - action: replace
          replacement: {{ .Environment.Name }}
          targetLabel: env
        - action: replace
          replacement: {{ .Environment.Name }}
          targetLabel: environment
        - sourceLabels: [__metrics_path__]
          targetLabel: metrics_path
      namespaceSelector:
        matchNames:
        - monitoring
      selector:
        matchLabels:
          app.kubernetes.io/name: kube-state-metrics

  # Gateway Nginx Exporter serviceMonitor
  - apiVersion: v1
    kind: Service
    metadata:
      labels:
        app.kubernetes.io/component: gateway-metrics
        app.kubernetes.io/instance: mimir
        app.kubernetes.io/name: mimir
      name: mimir-gateway-metrics
    spec:
      ports:
      - name: metrics
        port: 9113
        protocol: TCP
        targetPort: metrics
      selector:
        app.kubernetes.io/component: gateway
        app.kubernetes.io/instance: mimir
        app.kubernetes.io/name: mimir
      type: ClusterIP
      clusterIP: None

  - apiVersion: monitoring.coreos.com/v1
    kind: ServiceMonitor
    metadata:
      name: mimir-gateway
      labels:
        app.kubernetes.io/instance: mimir
        app.kubernetes.io/name: mimir
        app.kubernetes.io/component: gateway
    spec:
      endpoints:
      - port: metrics
        relabelings:
        - action: replace
          replacement: mimir/gateway
          sourceLabels:
          - job
          targetLabel: job
        - action: replace
          replacement: mimir
          targetLabel: cluster
        - action: replace
          replacement: mimir
          targetLabel: type
        scheme: http
      namespaceSelector:
        matchNames:
        - mimir
      selector:
        matchExpressions:
        - key: prometheus.io/service-monitor
          operator: NotIn
          values:
          - "false"
        matchLabels:
          app.kubernetes.io/component: gateway-metrics
          app.kubernetes.io/instance: mimir
          app.kubernetes.io/name: mimir

  - apiVersion: monitoring.coreos.com/v1
    kind: ServiceMonitor
    metadata:
      name: mimir-keda-metrics
      labels:
        app.kubernetes.io/instance: mimir
        app.kubernetes.io/name: mimir
        app.kubernetes.io/component: keda-metrics
    spec:
      endpoints:
      - port: metrics
        metricRelabelings:
        - action: keep
          regex: (.*)mimir(.*)
          separator: ;
          sourceLabels:
          - scaledObject
        relabelings:
        - action: keep
          sourceLabels:
          - __name__
          regex: "keda_scaler_metrics_value|keda_scaler_metrics_latency_seconds|keda_scaler_detail_errors_total"
        - action: replace
          replacement: mimir/keda-metrics
          sourceLabels:
          - job
          targetLabel: job
        - action: replace
          replacement: mimir
          targetLabel: cluster
        - action: replace
          replacement: mimir
          targetLabel: type
        scheme: http
      namespaceSelector:
        matchNames:
        - keda
      selector:
        matchLabels:
          app.kubernetes.io/component: operator
          app.kubernetes.io/instance: keda
          app.kubernetes.io/name: keda-operator-metrics-apiserver
