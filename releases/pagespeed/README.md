# pagespeed release

## Development

Create `private/secrets.yaml` with structure:

```
---
GOOGLE_API_KEY: "<key>"
```

This key is necessary to authenticate with the pagespeed API, you can generate your own key here https://developers.google.com/speed/docs/insights/v2/first-app or leave it blank though having a key is necessary for more frequent API requests.

Note: The key we use for the `gitlab-ops` project located in gkms, as well as in 1Password under `Page speed insights api key for gitlab-ops`

Run with the "minikube" Helmfile environment, e.g. `helmfile -e minikube apply`.
