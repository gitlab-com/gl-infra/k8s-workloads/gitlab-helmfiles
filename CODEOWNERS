# https://docs.gitlab.com/ee/user/project/code_owners.html

# Default approvers
* @gitlab-com/gl-infra

# For Renovate MR assignments
^[CI]
ci/ @gitlab-org/production-engineering/foundations
.gitlab-ci.yml @gitlab-org/production-engineering/foundations
.tool-versions @gitlab-org/production-engineering/foundations

^[Helm]
helmfile.yaml @gitlab-org/production-engineering/foundations @nduff
bases/ @gitlab-org/production-engineering/foundations @nduff
releases/ @gitlab-org/production-engineering/foundations @nduff

^[Helm: Data]
releases/airflow/ @gitlab-data/engineers
bases/environments/gitlab-analysis-prod.yaml @gitlab-data/engineers
bases/environments/gitlab-analysis-staging.yaml @gitlab-data/engineers

^[Helm: Database]
releases/cloudnative-pg-database-cluster/ @gitlab-org/database-team
releases/cloudnative-pg-operator/ @gitlab-org/database-team
releases/edb-postgresql-operator/ @gitlab-org/database-team

^[Helm: Foundations]
charts/ @gitlab-org/production-engineering/foundations
releases/10-gitlab-storage/ @gitlab-org/production-engineering/foundations
releases/20-gitlab-priority-classes/ @gitlab-org/production-engineering/foundations
releases/atlantis/ @gitlab-org/production-engineering/foundations
releases/calico-node-autoscaler/ @gitlab-org/production-engineering/foundations
releases/cert-manager/ @gitlab-org/production-engineering/foundations
releases/consul/ @gitlab-org/production-engineering/foundations
releases/external-dns/ @gitlab-org/production-engineering/foundations
releases/external-secrets/ @gitlab-org/production-engineering/foundations
releases/gateway/ @gitlab-org/production-engineering/foundations
releases/gitlab/ @gitlab-org/production-engineering/foundations
releases/gitlab-agent/ @gitlab-org/production-engineering/foundations
releases/gitlab-routes/ @gitlab-org/production-engineering/foundations
releases/istio/ @gitlab-org/production-engineering/foundations
releases/keda/ @gitlab-org/production-engineering/foundations
releases/kiali/ @gitlab-org/production-engineering/foundations
releases/kube-dns/ @gitlab-org/production-engineering/foundations
releases/oauth2-proxy/ @gitlab-org/production-engineering/foundations
releases/sysctl/ @gitlab-org/production-engineering/foundations
releases/reloader/ @gitlab-org/production-engineering/foundations
releases/teleport-agent/ @gitlab-org/production-engineering/foundations
releases/teleport-cluster/ @gitlab-org/production-engineering/foundations
releases/vault-k8s-secrets/ @gitlab-org/production-engineering/foundations
releases/vault/ @gitlab-org/production-engineering/foundations

^[Helm: Infrastructure Security]
releases/wiz-sensor @gitlab-com/gl-security/security-operations/infrastructure-security

^[Helm: Ops]
releases/camoproxy/ @gitlab-org/production-engineering/ops
releases/db-snowplow/ @gitlab-org/production-engineering/ops
releases/packagecloud/ @gitlab-org/production-engineering/ops

^[Helm: Observability]
releases/30-gitlab-monitoring/ @gitlab-org/scalability/observability
releases/clickhouse-operator/ @gitlab-org/scalability/observability
releases/cloudflare-exporter/ @gitlab-org/scalability/observability
releases/consul-exporter/ @gitlab-org/scalability/observability
releases/elasticsearch-exporter/ @gitlab-org/scalability/observability
releases/es-diagnostics/ @gitlab-org/scalability/observability
releases/gitaly-exporter/ @gitlab-org/scalability/observability
releases/grafana/ @gitlab-org/scalability/observability
releases/imap-mailbox-exporter/ @gitlab-org/scalability/observability
releases/loki/ @gitlab-org/scalability/observability
releases/mailgun-exporter/ @gitlab-org/scalability/observability
releases/mimir/ @gitlab-org/scalability/observability
releases/pagerduty-exporter/ @gitlab-org/scalability/observability
releases/pagespeed/ @gitlab-org/scalability/observability
releases/pingdom-exporter/ @gitlab-org/scalability/observability
releases/prometheus-process-exporter/ @gitlab-org/scalability/observability
releases/prometheus-scrape-configs/ @gitlab-org/scalability/observability
releases/promlens/ @gitlab-org/scalability/observability
releases/promtail/ @gitlab-org/scalability/observability
releases/pubsubbeat/ @gitlab-org/scalability/observability
releases/runway-exporter/ @gitlab-org/scalability/observability
releases/sentry/ @gitlab-org/scalability/observability
releases/stackdriver-exporter/ @gitlab-org/scalability/observability
releases/thanos/ @gitlab-org/scalability/observability

^[Helm: Practices]
releases/gitalyctl/ @gitlab-org/scalability/practices
