# simpleapp chart

A helm chart for reasonably common workloads: a deployment, an optional service,
and some configuration.

We shouldn't try to cram the kitchen sink into this chart. Anything reasonably
complex that we find ourselves needing to build a helm chart for, should have
its own helm chart.

## Usage

See [values.yaml](values.yaml) for a full list of configuration options.
